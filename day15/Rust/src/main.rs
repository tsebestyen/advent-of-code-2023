use std::fs;

fn part_one(values: &Vec<&str>) {
    println!("{}", values.iter().map(|v| {
        let mut current = 0;
        v.chars().into_iter().for_each(|c| {
            current = ((current + (c as usize)) * 17) % 256;
        });
        current
    }).sum::<usize>());
}

fn part_two(values: &Vec<&str>) {
    let mut boxes: [Vec<Lens>; 256] = [(); 256].map(|_| Vec::new());
    for value in values {
        let label = value.split(|c| c == '=' || c == '-').collect::<Vec<_>>()[0];
        let operator = if value.contains('=') { '=' } else { '-' };
        let number = &value[value.chars().position(|c| c == operator).unwrap() + 1..].parse().unwrap_or(0);
        let mut current = 0;
        for c in label.chars() {
            let ascii = c as usize;
            current += ascii;
            current *= 17;
            current %= 256;
        }
        if operator == '=' {
            let mut found = false;
            for lens in &mut boxes[current] {
                if lens.label == label {
                    lens.focal = *number;
                    found = true;
                }
            }
            if !found {
                boxes[current].push(Lens { label: label.to_string(), focal: *number });
            }
        } else if operator == '-' {
            for i in (0..boxes[current].len()).rev() {
                if boxes[current][i].label == label {
                    boxes[current].remove(i);
                }
            }
        }
    }
    println!("{}", (0..256).map(|b| {
        boxes[b].iter().enumerate().map(|(i, l)| (b + 1) * ((i + 1) * l.focal)).sum::<usize>()
    }).sum::<usize>());
}

struct Lens {
    label: String,
    focal: usize
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let values: Vec<&str> = data.trim().split(",").collect();
    // part_one(&values);  // 512283
    part_two(&values);     // 215827
}
