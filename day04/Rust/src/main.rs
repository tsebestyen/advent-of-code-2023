use std::fs;
use std::collections::{HashSet, VecDeque};

fn part_one(lines: &Vec<&str>) {
    let mut result = 0;
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let colon = line.find(":").unwrap();
        let parts: Vec<&str> = line.split("|").collect();
        let winning_numbers: HashSet<i32> = parts[0][colon + 1..].split(" ").filter(|v| !v.is_empty()).collect::<Vec<&str>>().iter().map(|v| v.parse().unwrap()).collect();
        let my_numbers: HashSet<i32> = parts[1].split(" ").filter(|v| !v.is_empty()).collect::<Vec<&str>>().iter().map(|v| v.trim().parse().unwrap()).collect();

        let num_common_numbers = winning_numbers.intersection(&my_numbers).count() as u32;

        if num_common_numbers > 0 {
            let value: i32 = 2_i32.pow(num_common_numbers - 1);
            result += value;
        }
    }
    println!("{}", result);
}

fn part_two(lines: &Vec<&str>) {
    let mut all_cards = Vec::new();
    let mut to_process = VecDeque::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        to_process.push_back(line);
    }

    while to_process.len() > 0 {
        let line = to_process.pop_front().unwrap();
        let colon = line.find(":").unwrap();
        let card_number: usize = line[5..colon].trim().parse().unwrap();
        all_cards.push(line);
        let parts: Vec<&str> = line.split("|").collect();
        let winning_numbers: HashSet<i32> = parts[0][colon + 1..].split(" ").filter(|v| !v.is_empty()).collect::<Vec<&str>>().iter().map(|v| v.parse().unwrap()).collect();
        let my_numbers: HashSet<i32> = parts[1].split(" ").filter(|v| !v.is_empty()).collect::<Vec<&str>>().iter().map(|v| v.trim().parse().unwrap()).collect();
        let num_common_numbers = winning_numbers.intersection(&my_numbers).count() as usize;
        for i in 0..num_common_numbers {
            to_process.push_back(&lines[card_number + i]);
        }
    }

    let result = all_cards.len();
    println!("{}", result);
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    // part_one(&lines);   // 21105
    part_two(&lines);      // 5329815
}
