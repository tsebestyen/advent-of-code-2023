use std::fs;
use std::collections::{HashMap, VecDeque};
use num::integer::lcm;

fn part_one(lines: &Vec<&str>) {
    let mut modules = HashMap::new();
    // Parse modules
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (module_str, destinations_str) = line.split_once(" -> ").unwrap();
        let name;
        let kind;
        if module_str == "broadcaster" {
            name = module_str.to_string();
            kind = ModuleType::Broadcaster;
        } else {
            name = module_str[1..].to_string();
            kind = match module_str.chars().nth(0).unwrap() {
                '%' => ModuleType::FlipFlop,
                '&' => ModuleType::Conjunction,
                _ => unreachable!("Ooops!")
            };
        }
        let destinations: Vec<_> = destinations_str.split(", ").collect();
        if destinations.contains(&"output") {
            modules.insert("output".to_string(), Module::new("output".to_string(), ModuleType::Output, vec![]));
        }
        if destinations.contains(&"rx") {
            modules.insert("rx".to_string(), Module::new("rx".to_string(), ModuleType::Output, vec![]));
        }
        modules.insert(name.clone(), Module::new(name, kind, destinations));
    }

    let modules_clone = modules.clone();
    modules.iter_mut().filter(|m| m.1.kind == ModuleType::Conjunction).for_each(|m| {
        for o in modules_clone.iter() {
            if o.1.destinations.contains(&m.0.as_str()) {
                m.1.signals.insert(o.0, Signal::Low);
            }
        }
    });

    let mut global_high = 0;
    let mut global_low = 0;
    for _ in 0..1000 {
        let mut q = VecDeque::new();
        q.push_back("broadcaster");

        //println!("button -Low-> broadcaster");

        let mut low_signals = 1;
        let mut high_signals = 0;
        while !q.is_empty() {
            let n = q.pop_front().unwrap();
            let mut m = modules.remove(n).unwrap();
            for d in &m.destinations {
                // if !modules.contains_key(*d) {
                //     println!("{}", d);
                // }
                let mut dm = modules.remove(*d).unwrap();
                // println!("{} -{:?}-> {}", m.name, m.output, dm.name);
                let mut send = true;
                match dm.kind {
                    ModuleType::Broadcaster => (),
                    ModuleType::FlipFlop => {
                        match m.output {
                            Signal::Low => {
                                if dm.state == State::Off {
                                    dm.state = State::On;
                                    dm.output = Signal::High;
                                } else {
                                    dm.state = State::Off;
                                    dm.output = Signal::Low;
                                }
                            },
                            Signal::High => {
                                send = false;
                            }
                        }
                    },
                    ModuleType::Conjunction => {
                        dm.signals.insert(n, m.output);
                        dm.output = if dm.signals.iter().all(|(_, &s)| s == Signal::High) { Signal::Low } else { Signal::High };
                    },
                    ModuleType::Output => ()
                }
                if m.output == Signal::High {
                    high_signals += 1;
                } else {
                    low_signals += 1;
                }
                modules.insert(d.to_string(), dm);
                if send {
                    q.push_back(d);
                }
            }
            modules.insert(n.to_string(), m);
        }
        global_high += high_signals;
        global_low += low_signals;
        // println!("High {}, Low {}", global_high, global_low);
        // println!("");
    }
    println!("High {}, Low {}", global_high, global_low);
    println!("{}", global_high * global_low);
}

fn part_two(lines: &Vec<&str>) {

    // println!("{}", vec![3907, 3889, 4003, 3911].iter().fold(1_i128, |acc, x| lcm(acc, *x)));
    // return;

    let mut modules = HashMap::new();
    // Parse modules
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (module_str, destinations_str) = line.split_once(" -> ").unwrap();
        let name;
        let kind;
        if module_str == "broadcaster" {
            name = module_str.to_string();
            kind = ModuleType::Broadcaster;
        } else {
            name = module_str[1..].to_string();
            kind = match module_str.chars().nth(0).unwrap() {
                '%' => ModuleType::FlipFlop,
                '&' => ModuleType::Conjunction,
                _ => unreachable!("Ooops!")
            };
        }
        let destinations: Vec<_> = destinations_str.split(", ").collect();
        if destinations.contains(&"output") {
            modules.insert("output".to_string(), Module::new("output".to_string(), ModuleType::Output, vec![]));
        }
        if destinations.contains(&"rx") {
            modules.insert("rx".to_string(), Module::new("rx".to_string(), ModuleType::Output, vec![]));
        }
        modules.insert(name.clone(), Module::new(name, kind, destinations));
    }

    let modules_clone = modules.clone();
    modules.iter_mut().filter(|m| m.1.kind == ModuleType::Conjunction).for_each(|m| {
        for o in modules_clone.iter() {
            if o.1.destinations.contains(&m.0.as_str()) {
                m.1.signals.insert(o.0, Signal::Low);
            }
        }
    });

    let mut num_buttons = 0;
    let mut rx_reached = false;
    while !rx_reached {
        num_buttons += 1;
        let mut q = VecDeque::new();
        q.push_back("broadcaster");

        //println!("button -Low-> broadcaster");

        while !q.is_empty() && !rx_reached {
            let n = q.pop_front().unwrap();
            let m = modules.remove(n).unwrap();
            for d in &m.destinations {
                let mut dm = modules.remove(*d).unwrap();
                // println!("{} -{:?}-> {}", m.name, m.output, dm.name);
                
                // if dm.name == "jq" {
                //     println!("Module: {}, Button: {}, Signal: {:?}", m.name, num_buttons, m.output);
                //     println!("jq: {:?}", dm.signals);
                // }

                // if m.output == Signal::High && vec!["vr", "lr", "nl", "gt"].contains(&m.name.as_str()) {
                //     println!("Module: {}, Button: {}, Signal: {:?}", m.name, num_buttons, m.output);
                //     let in_cycle = match m.name.as_str() {
                //         "vr" => num_buttons % 3907 == 0,
                //         "lr" => (num_buttons - 4) % 3885 == 0,
                //         "nl" => num_buttons % 4003 == 0,
                //         "gt" => num_buttons % 3911 == 0,
                //         _ => unreachable!("")
                //     };
                //     //println!("In cycle: {}", in_cycle);
                //     // vr: 3907
                //     // lr: 3889
                //     // nl: 4003
                //     // gt: 3911
                // }

                if dm.name == "rx" && m.output == Signal::Low {
                    rx_reached = true;
                    break;
                }
                
                let mut send = true;
                match dm.kind {
                    ModuleType::Broadcaster => (),
                    ModuleType::FlipFlop => {
                        match m.output {
                            Signal::Low => {
                                if dm.state == State::Off {
                                    dm.state = State::On;
                                    dm.output = Signal::High;
                                } else {
                                    dm.state = State::Off;
                                    dm.output = Signal::Low;
                                }
                            },
                            Signal::High => {
                                send = false;
                            }
                        }
                    },
                    ModuleType::Conjunction => {
                        dm.signals.insert(n, m.output);
                        dm.output = if dm.signals.iter().all(|(_, &s)| s == Signal::High) { Signal::Low } else { Signal::High };
                    },
                    ModuleType::Output => ()
                }
                modules.insert(d.to_string(), dm);
                if send {
                    q.push_back(d);
                }
            }
            modules.insert(n.to_string(), m);
        }
    }
    println!("{}", num_buttons);
}

#[derive(Debug, Clone)]
struct Module<'a> {
    name: String,
    kind: ModuleType,
    output: Signal,
    state: State,
    signals: HashMap<&'a str, Signal>,
    destinations: Vec<&'a str>
}

impl Module<'_> {
    fn new(name: String, kind: ModuleType, destinations: Vec<&str>) -> Module<'_> {
        Module {
            name: name,
            kind: kind,
            output: Signal::Low,
            state: State::Off,
            signals: HashMap::new(),
            destinations: destinations
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ModuleType {
    Broadcaster,
    FlipFlop,
    Conjunction,
    Output
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum State {
    On,
    Off
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Signal {
    Low,
    High
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    // part_one(&lines);       // 912199500
    part_two(&lines);          // 237633596208435: ko, 237633596208434: ko, 237878264003759: OK
}
