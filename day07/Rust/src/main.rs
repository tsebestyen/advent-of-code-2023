use std::cmp::Ordering;
use std::fs;
use std::collections::HashMap;

fn part_one(lines: &Vec<&str>) {
    let labels = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'];
    let mut hands = Vec::new();
    // Parse lines into Hands
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let tokens: Vec<_> = line.split_whitespace().collect();
        hands.push(Hand { hand: tokens[0].to_string(), bid: tokens[1].parse().unwrap()})
    }
    // Sort the hands by type and rank
    hands.sort_by(|a, b| {
        let a_type = get_type(&a.hand);
        let b_type = get_type(&b.hand);
        if a_type != b_type {
            return b_type.cmp(&a_type);
        }
        for (ac, bc) in a.hand.chars().zip(b.hand.chars()) {
            if ac == bc {
                continue;
            }
            let ac_value = labels.len() - labels.iter().position(|&x| x == ac).unwrap();
            let bc_value = labels.len() - labels.iter().position(|&x| x == bc).unwrap();
            return bc_value.cmp(&ac_value);
        }
        Ordering::Greater
    });
    let result: usize = hands.iter().rev().enumerate().map(|(i, h)| (i + 1) * h.bid).sum();
    println!("{:?}", result);
}

fn part_two(lines: &Vec<&str>) {
    let labels = ['A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J'];
    let mut hands = Vec::new();
    // Parse lines into Hands
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let tokens: Vec<_> = line.split_whitespace().collect();
        hands.push(Hand { hand: tokens[0].to_string(), bid: tokens[1].parse().unwrap()})
    }
    // Sort the hands by type and rank
    hands.sort_by(|a, b| {
        let a_type = get_type_v2(&a.hand);
        let b_type = get_type_v2(&b.hand);
        if a_type != b_type {
            return b_type.cmp(&a_type);
        }
        for (ac, bc) in a.hand.chars().zip(b.hand.chars()) {
            if ac == bc {
                continue;
            }
            let ac_value = labels.len() - labels.iter().position(|&x| x == ac).unwrap();
            let bc_value = labels.len() - labels.iter().position(|&x| x == bc).unwrap();
            return bc_value.cmp(&ac_value);
        }
        Ordering::Greater
    });
    // for hand in &hands {
    //     println!("{:?}", hand);
    // }
    // println!("{:?}", hands.iter().map(|h| get_type_v2(&h.hand)).collect::<Vec<_>>());
    let result: usize = hands.iter().rev().enumerate().map(|(i, h)| (i + 1) * h.bid).sum();
    println!("{:?}", result);
}

#[derive(Debug)]
struct Hand {
    hand: String,
    bid: usize
}

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
enum Type {
    FiveOfAKind = 6,
    FourOfAKind = 5,
    FullHouse = 4,
    ThreeOfAKind = 3,
    TwoPair = 2,
    OnePair = 1,
    HighCard = 0
}

fn get_type(hand: &str) -> Type {
    let mut cards = HashMap::new();
    for card in hand.chars() {
        *cards.entry(card).or_insert(0) += 1;
    }
    let mut num_pairs = 0;
    let mut has_three = false;
    for key in cards.keys() {
        if cards[&key] == 5 {
            return Type::FiveOfAKind;
        }
        if cards[&key] == 4 {
            return Type::FourOfAKind;
        }
        if cards[&key] == 3 {
            has_three = true;
        }
        if cards[&key] == 2 {
            num_pairs += 1;
        }
    }
    if has_three && num_pairs > 0 {
        return Type::FullHouse;
    }
    if has_three {
        return Type::ThreeOfAKind;
    }
    if num_pairs == 2 {
        return Type::TwoPair;
    }
    if num_pairs > 0 {
        return Type::OnePair;
    }
    Type::HighCard
}

fn get_type_v2(hand: &str) -> Type {
    let mut cards = HashMap::new();
    for card in hand.chars() {
        if card == 'J' {
            continue;
        }
        *cards.entry(card).or_insert(0) += 1;
    }
    let num_jokers: usize = hand.chars().filter(|c| c == &'J').count();
    let mut num_pairs = 0;
    let mut has_three = false;
    for key in cards.keys() {
        if cards[&key] == 5 {
            return Type::FiveOfAKind;
        }
        if cards[&key] == 4 && num_jokers > 0 {
            return Type::FiveOfAKind;
        }
        if cards[&key] == 4 {
            return Type::FourOfAKind;
        }
        if cards[&key] == 3 {
            has_three = true;
        }
        if cards[&key] == 2 {
            num_pairs += 1;
        }
    }
    if has_three && num_jokers == 2 {
        return Type::FiveOfAKind;
    }
    if has_three && num_jokers == 1 {
        return  Type::FourOfAKind;
    }
    if has_three && num_pairs > 0 {
        return Type::FullHouse;
    }
    if has_three && num_jokers > 0 {
        return Type::FullHouse;
    }
    if has_three {
        return Type::ThreeOfAKind;
    }
    if num_pairs == 2 && num_jokers > 0 {
        return Type::FullHouse;
    }
    if num_pairs == 2 {
        return Type::TwoPair;
    }
    if num_pairs > 0 && num_jokers == 3 {
        return Type::FiveOfAKind;
    }
    if num_pairs > 0 && num_jokers == 2 {
        return Type::FourOfAKind;
    }
    if num_pairs > 0 && num_jokers == 1 {
        return Type::ThreeOfAKind;
    }
    if num_pairs > 0 {
        return Type::OnePair;
    }
    if num_jokers == 5 {
        return Type::FiveOfAKind;
    }
    if num_jokers == 4 {
        return Type::FiveOfAKind;
    }
    if num_jokers == 3 {
        return Type::FourOfAKind;
    }
    if num_jokers == 2 {
        return Type::ThreeOfAKind;
    }
    if num_jokers == 1 {
        return Type::OnePair;
    }
    Type::HighCard
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    
    // part_one(&lines);   // 250370832: KO, 251216224
    part_two(&lines);      // 250332347: ko, 250154712: ko, 250825971, 250825971
}
