use std::fs;
use std::collections::{HashMap, HashSet};

fn part_one(lines: &Vec<&str>) {
    let mut bricks = Vec::new();
    let mut grid = HashSet::new();
    // Parse bricks
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (start, end) = line.split_once("~").unwrap();
        let coords0: Vec<_> = start.splitn(3, ",").map(|c| c.parse().unwrap()).collect();
        let coords1: Vec<_> = end.splitn(3, ",").map(|c| c.parse().unwrap()).collect();
        let mut o = Orientation::X;
        if coords0[1] != coords1[1] {
            o = Orientation::Y;
        }
        if coords0[2] != coords1[2] {
            o = Orientation::Z;
        }
        let b = Brick {
            x0: coords0[0],
            y0: coords0[1],
            z0: coords0[2],
            x1: coords1[0],
            y1: coords1[1],
            z1: coords1[2],
            orientation: o
        };
        if b.y0 == b.y1 && b.z0 == b.z1 {
            for i in 0..=b.x1 - b.x0 {
                grid.insert((b.x0 + i, b.y0, b.z0));
            }
        }
        if b.x0 == b.x1 && b.z0 == b.z1 {
            for i in 0..=b.y1 - b.y0 {
                grid.insert((b.x0, b.y0 + i, b.z0));
            }
        }
        if b.x0 == b.x1 && b.y0 == b.y1 {
            for i in 0..=b.z1 - b.z0 {
                grid.insert((b.x0, b.y0, b.z0 + i));
            }
        }
        bricks.push(b);
    }

    // let mut view_xz = Vec::new();
    // (0..10).for_each(|_| view_xz.push(String::from("...")));
    // for c in &grid {
    //     view_xz[c.2 as usize].replace_range(c.0 as usize..c.0 as usize + 1, "#");
    // }
    // for row in view_xz.iter().rev() {
    //     println!("{}", row);
    // }

    // Order the bricks by Z so they are in order before falling
    bricks.sort_by(|a, b| a.z0.cmp(&b.z0));

    // Fall the bricks
    for i in 0..bricks.len() {
        let b = &mut bricks[i];
        loop {
            if b.z0 == 1 {
                break;
            }
            if b.orientation != Orientation::Z && b.iter().any(|c| grid.contains(&(c.0, c.1, c.2 - 1))) {
                break;
            }
            if b.orientation == Orientation::Z && grid.contains(&(b.x0, b.y0, b.z0 - 1)) {
                break;
            }
            // Remove all the brick's coords
            for c in b.iter() {
                grid.remove(&c);
            }
            // Make brick z one less
            b.z0 -= 1;
            b.z1 -= 1;
            // Add new brick to grid
            for c in b.iter() {
                grid.insert(c);
            }
        }
    }

    // let mut view_xz = Vec::new();
    // (0..320).for_each(|_| view_xz.push(String::from("..........")));
    // for c in &grid {
    //     view_xz[c.2 as usize].replace_range(c.0 as usize..c.0 as usize + 1, "#");
    // }
    // for row in view_xz.iter().rev() {
    //     println!("{}", row);
    // }
    
    let mut supports = HashMap::<Brick, HashSet<&Brick>>::new();
    let mut supported_by = HashMap::<Brick, HashSet<&Brick>>::new();
    for this_brick in &bricks {
        let bc: Vec<_> = this_brick.iter().map(|c| (c.0, c.1)) .collect();
        let supported_bricks: Vec<_> = bricks.iter()
            .filter(|&ob| ob != this_brick)
            .filter(|&ob| ob.z0 == this_brick.z1 + 1)
            .filter(|&ob| {
                let obc: Vec<_> = ob.iter().collect();
                if obc.iter().any(|c| bc.contains(&(c.0, c.1))) {
                    return true;
                }
                false
            }).collect();
        supported_bricks.iter().for_each(|&sb| {
            if supports.contains_key(this_brick) {
                supports.get_mut(this_brick).unwrap().insert(sb);
            } else {
                let mut bhs = HashSet::new();
                bhs.insert(sb);
                supports.insert(this_brick.clone(), bhs);
            }
            if supported_by.contains_key(sb) {
                supported_by.get_mut(sb).unwrap().insert(this_brick);
            } else {
                let mut bhs = HashSet::new();
                bhs.insert(this_brick);
                supported_by.insert(sb.clone(), bhs);
            }
        });
    }

    let bricks_doesnt_support = bricks.len() - supports.len();

    // Bricks that support other bricks, but all other bricks have other support
    let safe_bricks = &supports.iter().filter(|&(_, supported_bricks)| {
        if supported_bricks.iter().all(|supported_brick| {
            supported_by.get(&supported_brick).unwrap().len() > 1
        }) {
            return true;
        }
        false
    }).count();

    println!("{}", bricks_doesnt_support + safe_bricks);
}

fn part_two(lines: &Vec<&str>) {
    let mut bricks = Vec::new();
    let mut grid = HashSet::new();
    // Parse bricks
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (start, end) = line.split_once("~").unwrap();
        let coords0: Vec<_> = start.splitn(3, ",").map(|c| c.parse().unwrap()).collect();
        let coords1: Vec<_> = end.splitn(3, ",").map(|c| c.parse().unwrap()).collect();
        let mut o = Orientation::X;
        if coords0[1] != coords1[1] {
            o = Orientation::Y;
        }
        if coords0[2] != coords1[2] {
            o = Orientation::Z;
        }
        let b = Brick {
            x0: coords0[0],
            y0: coords0[1],
            z0: coords0[2],
            x1: coords1[0],
            y1: coords1[1],
            z1: coords1[2],
            orientation: o
        };
        if b.y0 == b.y1 && b.z0 == b.z1 {
            for i in 0..=b.x1 - b.x0 {
                grid.insert((b.x0 + i, b.y0, b.z0));
            }
        }
        if b.x0 == b.x1 && b.z0 == b.z1 {
            for i in 0..=b.y1 - b.y0 {
                grid.insert((b.x0, b.y0 + i, b.z0));
            }
        }
        if b.x0 == b.x1 && b.y0 == b.y1 {
            for i in 0..=b.z1 - b.z0 {
                grid.insert((b.x0, b.y0, b.z0 + i));
            }
        }
        bricks.push(b);
    }

    // Order the bricks by Z so they are in order before falling
    bricks.sort_by(|a, b| a.z0.cmp(&b.z0));

    fall_bricks(&mut bricks, &mut grid);

    let mut result = 0;
    for i in 0..bricks.len() {
        let mut new_bricks = bricks.clone();
        let mut new_grid = grid.clone();
        let b = new_bricks.remove(i);
        for c in b.iter() {
            new_grid.remove(&c);
        }
        let bricks_fallen = fall_bricks(&mut new_bricks, &mut new_grid);
        result += bricks_fallen;
    }

    println!("{}", result);
}

fn fall_bricks(bricks: &mut Vec<Brick>, grid: &mut HashSet<(i32, i32, i32)>) -> i32 {
    let mut bricks_fallen = 0;
    for i in 0..bricks.len() {
        let b = &mut bricks[i];
        let mut this_brick_fell = false;
        loop {
            if b.z0 == 1 {
                break;
            }
            if b.orientation != Orientation::Z && b.iter().any(|c| grid.contains(&(c.0, c.1, c.2 - 1))) {
                break;
            }
            if b.orientation == Orientation::Z && grid.contains(&(b.x0, b.y0, b.z0 - 1)) {
                break;
            }
            // Remove all the brick's coords
            for c in b.iter() {
                grid.remove(&c);
            }
            // Make brick z one less
            b.z0 -= 1;
            b.z1 -= 1;
            // Add new brick to grid
            for c in b.iter() {
                grid.insert(c);
            }
            this_brick_fell = true;
        }
        if this_brick_fell {
            bricks_fallen += 1
        }
    }
    bricks_fallen
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
enum Orientation {
    X,
    Y,
    Z
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Brick {
    x0: i32,
    y0: i32,
    z0: i32,
    x1: i32,
    y1: i32,
    z1: i32,
    orientation: Orientation
}

struct BrickIterator<'a> {
    brick: &'a Brick,
    index: usize
}

impl<'a> Iterator for BrickIterator<'a> {
    type Item = (i32, i32, i32);

    fn next(&mut self) -> Option<Self::Item> {
        if self.brick.y0 == self.brick.y1 && self.brick.z0 == self.brick.z1 {
            if (self.index as i32) <= self.brick.x1 - self.brick.x0 {
                let result = Some((self.brick.x0 + self.index as i32, self.brick.y0, self.brick.z0));
                self.index += 1;
                return result;
            }
        }
        if self.brick.x0 == self.brick.x1 && self.brick.z0 == self.brick.z1 {
            if (self.index as i32) <= self.brick.y1 - self.brick.y0 {
                let result = Some((self.brick.x0, self.brick.y0 + self.index as i32, self.brick.z0));
                self.index += 1;
                return result;
            }
        }
        if self.brick.x0 == self.brick.x1 && self.brick.y0 == self.brick.y1 {
            if (self.index as i32) <= self.brick.z1 - self.brick.z0 {
                let result = Some((self.brick.x0, self.brick.y0, self.brick.z0 + self.index as i32));
                self.index += 1;
                return result;
            }
        }
        None
    }
}

impl Brick {
    pub fn iter(&self) -> BrickIterator {
        BrickIterator {
            brick: self,
            index: 0,
        }
    }
}

fn main() {
    //let data = fs::read_to_string("sample_input").expect("Unable to read input");
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    // part_one(&lines);   // 620: KO, 254: ko, 398
    part_two(&lines);      // 70727
}
