use std::fs;
use std::collections::{HashSet, VecDeque};

fn part_one(lines: &Vec<&str>) {
    let path = Path { pos: (0, 1), v: HashSet::new() };
    //path.v.insert((0, 1));
    let goal = (lines.len() - 1, lines[0].len() - 2);
    let mut q = VecDeque::new();
    q.push_back(path);
    let mut paths = Vec::new();
    while !q.is_empty() {
        let n = q.pop_front().unwrap();
        if n.pos.1 > 0 {
            let mut new_pos = (n.pos.0, n.pos.1 - 1);
            if new_pos == goal {
                let mut new_n_v = n.v.clone();
                new_n_v.insert(new_pos);
                paths.push(Path { pos: new_pos, v: new_n_v });
                continue;
            }
            if !n.v.contains(&new_pos) {
                let tile = lines[new_pos.0].chars().nth(new_pos.1).unwrap();
                if tile == '.' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == '>' {
                    // not possible
                } else if tile == 'v' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0 + 1, new_pos.1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                }
            }
        }
        if n.pos.1 < lines[0].len() - 1 {
            let mut new_pos = (n.pos.0, n.pos.1 + 1);
            if new_pos == goal {
                let mut new_n_v = n.v.clone();
                new_n_v.insert(new_pos);
                paths.push(Path { pos: new_pos, v: new_n_v });
                continue;
            }
            if !n.v.contains(&new_pos) {
                let tile = lines[new_pos.0].chars().nth(new_pos.1).unwrap();
                if tile == '.' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == '>' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0, new_pos.1 + 1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == 'v' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0 + 1, new_pos.1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                }
            }
        }
        if n.pos.0 > 0 {
            let mut new_pos = (n.pos.0 - 1, n.pos.1);
            if new_pos == goal {
                let mut new_n_v = n.v.clone();
                new_n_v.insert(new_pos);
                paths.push(Path { pos: new_pos, v: new_n_v });
                continue;
            }
            if !n.v.contains(&new_pos) {
                let tile = lines[new_pos.0].chars().nth(new_pos.1).unwrap();
                if tile == '.' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == '>' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0, new_pos.1 + 1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == 'v' {
                    // not possible
                }
            }
        }
        if n.pos.0 < lines.len() - 1 {
            let mut new_pos = (n.pos.0 + 1, n.pos.1);
            if new_pos == goal {
                let mut new_n_v = n.v.clone();
                new_n_v.insert(new_pos);
                paths.push(Path { pos: new_pos, v: new_n_v });
                continue;
            }
            if !n.v.contains(&new_pos) {
                let tile = lines[new_pos.0].chars().nth(new_pos.1).unwrap();
                if tile == '.' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == '>' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0, new_pos.1 + 1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                } else if tile == 'v' {
                    let mut new_n_v = n.v.clone();
                    new_n_v.insert(new_pos);
                    new_pos = (new_pos.0 + 1, new_pos.1);
                    new_n_v.insert(new_pos);
                    let new_path = Path { pos: new_pos, v: new_n_v };
                    q.push_back(new_path);
                }
            }
        }
    }
    // for p in &paths {
    //     println!("Path length: {}", p.v.len());
    // }
    // let mut grid = Vec::new();
    // for (line_idx, line) in lines.iter().enumerate() {
    //     let mut row = String::with_capacity(line.len());
    //     for (c_idx, c) in line.chars().enumerate() {
    //         if paths[paths.len() - 1].v.contains(&(line_idx, c_idx)) {
    //             row.push('O');
    //         } else {
    //             row.push(c);
    //         }
    //     }
    //     grid.push(row);
    // }
    // for row in grid {
    //     println!("{}", row);
    // }
    println!("{}", paths.iter().map(|p| p.v.len()).max().unwrap());
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Path {
    pos: (usize, usize),
    v: HashSet<(usize, usize)>
}

fn main() {
    // let data = fs::read_to_string("sample_input").expect("Unable to read input");
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    part_one(&lines);   // 2306
}
