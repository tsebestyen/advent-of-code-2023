use std::fs;
use std::collections::HashMap;
use num::integer::lcm;

fn part_one(lines: &Vec<&str>) {
    let instructions = lines[0].trim();
    let mut map = HashMap::new();
    for line in &lines[2..] {
        if line.len() == 0 {
            continue;
        }
        let start = &line[..3];
        let left = &line[7..10];
        let right = &line[12..15];
        map.insert(start, (left, right));
    }
    let start = map["AAA"];
    let end = map["ZZZ"];
    let mut current = start;
    let mut num_steps = 0;
    let mut instruction_idx = 0;
    loop {
        if instruction_idx == instructions.len() {
            instruction_idx = 0;
        }
        let instruction = instructions.chars().nth(instruction_idx).unwrap();
        let destinations = current;
        let key = match instruction {
            'L' => destinations.0,
            'R' => destinations.1,
            _ => unreachable!("")
        };
        current = map[key];
        instruction_idx += 1;
        num_steps += 1;
        if current == end {
            break;
        }
    }
    println!("{}", num_steps);
}

fn part_two(lines: &Vec<&str>) {
    let instructions = lines[0].trim();
    let mut map = HashMap::new();
    for line in &lines[2..] {
        if line.len() == 0 {
            continue;
        }
        let start = &line[..3];
        let left = &line[7..10];
        let right = &line[12..15];
        map.insert(start, (left, right));
    }

    let mut current_positions: Vec<_> = map.iter().filter(|(k, _v)| k.ends_with("A")).map(|(k, _v)| *k).collect();

    // Get A-Z loop lengths for each position
    // Get the position where they first reach Z
    // Calculate (how?) when they all reach Z
    let mut loop_lengths = Vec::new();
    let mut first_z_steps = Vec::new();
    for current_position in current_positions.iter_mut() {
        let mut num_steps_first_z = 0;
        let mut loop_length = 0;
        let mut first_z_reached = false;
        let mut instruction_idx = 0;
        loop {
            if instruction_idx == instructions.len() {
                instruction_idx = 0;
            }
            let instruction = instructions.chars().nth(instruction_idx).unwrap();
            let destinations = map[current_position];
            let key = match instruction {
                'L' => destinations.0,
                'R' => destinations.1,
                _ => unreachable!("")
            };
            *current_position = key;
            instruction_idx += 1;
            num_steps_first_z += 1;
            if first_z_reached {
                loop_length += 1;
            }
            if current_position.ends_with("Z") && !first_z_reached {
                first_z_steps.push(num_steps_first_z);
                first_z_reached = true;
            } else if current_position.ends_with("Z") && first_z_reached {
                loop_lengths.push(loop_length);
                break;
            }
        }
    }
    println!("First Z {:?}, Loop {:?}", first_z_steps, loop_lengths);

    let result = first_z_steps.iter().fold(1_i128, |acc, x| lcm(acc, *x));
    println!("{}", result);
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    
    // part_one(&lines);   // 21251
    part_two(&lines);
}
