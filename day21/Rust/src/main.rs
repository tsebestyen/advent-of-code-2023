use std::fs;
use std::collections::{HashSet, VecDeque};

fn part_one(lines: &Vec<&str>) {
    let mut start_pos = (0, 0);
    for (line_idx, line) in lines.iter().enumerate() {
        for (c_idx, c) in line.chars().enumerate() {
            if c == 'S' {
                start_pos = (line_idx, c_idx);
                break;
            }
        }
        if start_pos != (0, 0) {
            break;
        }
    }
    let max_x = lines[0].len();
    let max_y = lines.len();
    let mut q = VecDeque::new();
    let mut q2 = VecDeque::new();
    q.push_back(start_pos);
    let mut o = HashSet::new();
    o.insert(start_pos);
    for s in 0..64 {
        //println!("Step {}: {}", s + 1, o.iter().count());
        while !q.is_empty() {
            let n = q.pop_front().unwrap();
            o.remove(&n);
            if n.1 > 0 {
                let new_pos = (n.0, n.1 - 1);
                if lines[n.0].chars().nth(n.1 - 1).unwrap() != '#' {
                    if !o.contains(&new_pos) {
                        q2.push_back(new_pos);
                        o.insert(new_pos);
                    }
                }
            }
            if n.1 < max_x - 1 {
                let new_pos = (n.0, n.1 + 1);
                if lines[n.0].chars().nth(n.1 + 1).unwrap() != '#' {
                    if !o.contains(&new_pos) {
                        q2.push_back(new_pos);
                        o.insert(new_pos);
                    }
                }
            }
            if n.0 > 0 {
                let new_pos = (n.0 -1, n.1);
                if lines[n.0 - 1].chars().nth(n.1).unwrap() != '#' {
                    if !o.contains(&new_pos) {
                        q2.push_back(new_pos);
                        o.insert(new_pos);
                    }
                }
            }
            if n.0 < max_y - 1 {
                let new_pos = (n.0 + 1, n.1);
                if lines[n.0 + 1].chars().nth(n.1).unwrap() != '#' {
                    if !o.contains(&new_pos) {
                        q2.push_back(new_pos);
                        o.insert(new_pos);
                    }
                }
            }
        }
        q = q2.clone();
        q2.clear();
    }
    println!("{}", o.iter().count());
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    part_one(&lines);       // 3617
}
