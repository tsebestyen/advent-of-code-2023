use std::fs;
use std::collections::HashMap;

fn part_one(lines: &Vec<&str>) {
    let calibration_value: u32 = lines
        .iter()
        .map(|line| {
            let first_digit: u32 = line
                .chars()
                .into_iter()
                .filter(|value| value.is_digit(10))
                .map(|c| c.to_digit(10).unwrap())
                .take(1)
                .sum();
            let last_digit: u32 = line
                .chars()
                .into_iter()
                .rev()
                .filter(|value| value.is_digit(10))
                .map(|c| c.to_digit(10).unwrap())
                .take(1)
                .sum();
            first_digit * 10 + last_digit
        }).sum();

    println!("{}", calibration_value);
}

fn part_two(lines: &Vec<&str>) {
    let digits = HashMap::from([
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9)
    ]);

    let mut calibration_value = 0;
    for line in lines {
        let mut first_digit = 0;
        let mut last_digit = 0;
        for (c_idx, c) in line.chars().enumerate() {
            if first_digit != 0 {
                break;
            }
            if c.is_digit(10) {
                first_digit = c.to_digit(10).unwrap();
                break;
            }
            for k in digits.keys() {
                if line[c_idx..].find(k) == Some(0) {
                    first_digit = digits[k];
                    break;
                }
            }
        }

        for (c_idx, c) in line.chars().rev().enumerate() {
            if last_digit != 0 {
                break;
            }
            if c.is_digit(10) {
                last_digit = c.to_digit(10).unwrap();
                break;
            }
            for k in digits.keys() {
                if line[line.len() - c_idx - 1..].find(k) == Some(0) {
                    last_digit = digits[k];
                    break;
                }
            }
        }
        let number = first_digit * 10 + last_digit;
        println!("{}", number);
        calibration_value += number;
    }

    println!("{}", calibration_value);
}

fn main() {
    let contents = fs::read_to_string("input")
        .expect("Should have been able to read the file");
    let lines: Vec<&str> = contents.split("\n").collect();
    
    // part_one(&lines);   // 56042
    part_two(&lines);      // 55358
}
