use std::fs;
use std::collections::{HashSet, VecDeque};

fn part_one(lines: &Vec<&str>) {
    let mut grid: Vec<Vec<char>> = Vec::new();
    let mut pos = (0_i32, 0_i32);
    let mut min_x = 0;
    let mut min_y = 0;
    let mut max_x = 0;
    let mut max_y = 0;
    for line in lines {
        let tokens: Vec<_> = line.split_whitespace().collect();
        let dir = match tokens[0] {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "R" => Direction::Right,
            "L" => Direction::Left,
            _ => unreachable!("Ooops!")
        };
        let value: i32 = tokens[1].parse().unwrap();
        match dir {
            Direction::Up => {
                pos = (pos.0, pos.1 - value);
                if pos.1 < min_y {
                    min_y = pos.1
                }
            },
            Direction::Down => {
                pos = (pos.0, pos.1 + value);
                if pos.1 > max_y {
                    max_y = pos.1
                }
            },
            Direction::Right => {
                pos = (pos.0 + value, pos.1);
                if pos.0 > max_x {
                    max_x = pos.0
                }
            },
            Direction::Left => {
                pos = (pos.0 - value, pos.1);
                if pos.0 < min_x {
                    min_x = pos.0
                }
            }
        }
    }
    // Prepare the grid
    for _ in min_y..=max_y {
        let mut row = Vec::new();
        for _ in min_x..=max_x {
            row.push('.');
        }
        grid.push(row);
    }
    pos = (0 - min_x, 0 - min_y);
    min_x = 0;
    min_y = 0;
    max_y = grid.len() as i32 - 1;
    max_x = grid[0].len() as i32 - 1;

    // Draw the grid
    for line in lines {
        let tokens: Vec<_> = line.split_whitespace().collect();
        let dir = match tokens[0] {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "R" => Direction::Right,
            "L" => Direction::Left,
            _ => unreachable!("Ooops!")
        };
        let value: i32 = tokens[1].parse().unwrap();
        match dir {
            Direction::Up => {
                for n in 0..=value {
                    grid[(pos.1 - n) as usize][pos.0 as usize] = '#';
                }
                pos = (pos.0, pos.1 - value);
                
            },
            Direction::Down => {
                for n in 0..=value {
                    grid[(pos.1 + n) as usize][pos.0 as usize] = '#';
                }
                pos = (pos.0, pos.1 + value);
                
            },
            Direction::Right => {
                for n in 0..=value {
                    grid[pos.1 as usize][(pos.0 + n) as usize] = '#';
                }
                pos = (pos.0 + value, pos.1);
            },
            Direction::Left => {
                for n in 0..=value {
                    grid[pos.1 as usize][(pos.0 - n) as usize] = '#';
                }
                pos = (pos.0 - value, pos.1);
                
            }
        }
    }

    // for row in &grid {
    //     println!("{}", row.iter().collect::<String>());
    // }

    let mut start = (0_i32, 0_i32);
    for i in 0..grid.len() {
        let p = grid[i].iter().collect::<String>().find(".#.");
        if p.is_some() {
            start = (p.unwrap() as i32 + 2, i as i32);
            break;
        }
    }

    flood_fill(&mut grid, start);

    // for row in &grid {
    //     println!("{}", row.iter().collect::<String>());
    // }

    println!("{}", grid.iter().map(|r| r.iter().filter(|&c| c != &'.').count()).sum::<usize>());
}

fn part_two(lines: &Vec<&str>) {
    let mut grid: Vec<Vec<char>> = Vec::new();
    let mut pos = (0_i32, 0_i32);
    let mut min_x = 0;
    let mut min_y = 0;
    let mut max_x = 0;
    let mut max_y = 0;
    for line in lines {
        let tokens: Vec<_> = line.split_whitespace().collect();
        let instruction = tokens[2][2..9].to_string();
        let dir = match instruction.chars().nth(5).unwrap() {
            '3' => Direction::Up,
            '1' => Direction::Down,
            '0' => Direction::Right,
            '2' => Direction::Left,
            _ => unreachable!("Ooops!")
        };
        let value = i32::from_str_radix(&instruction[..5], 16).unwrap();
        match dir {
            Direction::Up => {
                pos = (pos.0, pos.1 - value);
                if pos.1 < min_y {
                    min_y = pos.1
                }
            },
            Direction::Down => {
                pos = (pos.0, pos.1 + value);
                if pos.1 > max_y {
                    max_y = pos.1
                }
            },
            Direction::Right => {
                pos = (pos.0 + value, pos.1);
                if pos.0 > max_x {
                    max_x = pos.0
                }
            },
            Direction::Left => {
                pos = (pos.0 - value, pos.1);
                if pos.0 < min_x {
                    min_x = pos.0
                }
            }
        }
    }

    println!("Grid size: {}x{}", max_x-min_x, max_y-min_y);

    // Prepare the grid
    for _ in min_y..=max_y {
        let mut row = Vec::new();
        for _ in min_x..=max_x {
            row.push('.');
        }
        grid.push(row);
    }
    pos = (0 - min_x, 0 - min_y);
    min_x = 0;
    min_y = 0;
    max_y = grid.len() as i32 - 1;
    max_x = grid[0].len() as i32 - 1;

    // Draw the grid
    for line in lines {
        let tokens: Vec<_> = line.split_whitespace().collect();
        let instruction = tokens[2][2..9].to_string();
        let dir = match instruction.chars().nth(5).unwrap() {
            '3' => Direction::Up,
            '1' => Direction::Down,
            '0' => Direction::Right,
            '2' => Direction::Left,
            _ => unreachable!("Ooops!")
        };
        let value = i32::from_str_radix(&instruction[..5], 16).unwrap();
        match dir {
            Direction::Up => {
                for n in 0..=value {
                    grid[(pos.1 - n) as usize][pos.0 as usize] = '#';
                }
                pos = (pos.0, pos.1 - value);
                
            },
            Direction::Down => {
                for n in 0..=value {
                    grid[(pos.1 + n) as usize][pos.0 as usize] = '#';
                }
                pos = (pos.0, pos.1 + value);
                
            },
            Direction::Right => {
                for n in 0..=value {
                    grid[pos.1 as usize][(pos.0 + n) as usize] = '#';
                }
                pos = (pos.0 + value, pos.1);
            },
            Direction::Left => {
                for n in 0..=value {
                    grid[pos.1 as usize][(pos.0 - n) as usize] = '#';
                }
                pos = (pos.0 - value, pos.1);
                
            }
        }
    }

    println!("Grid drawn");

    // for row in &grid {
    //     println!("{}", row.iter().collect::<String>());
    // }

    let mut start = (0_i32, 0_i32);
    for i in 0..grid.len() {
        let p = grid[i].iter().collect::<String>().find(".#.");
        if p.is_some() {
            start = (p.unwrap() as i32 + 2, i as i32);
            break;
        }
    }

    flood_fill(&mut grid, start);

    // for row in &grid {
    //     println!("{}", row.iter().collect::<String>());
    // }

    println!("{}", grid.iter().map(|r| r.iter().filter(|&c| c != &'.').count()).sum::<usize>());
}

fn flood_fill(grid: &mut Vec<Vec<char>>, start: (i32, i32)) {
    let mut q = VecDeque::new();
    let mut v = HashSet::new();
    q.push_back(start);
    v.insert(start);
    while !q.is_empty() {
        let n = q.pop_front().unwrap();
        grid[n.1 as usize][n.0 as usize] = '$';
        if n.0 > 0 && grid[n.1 as usize][(n.0 - 1) as usize] == '.' {
            let new_pos = (n.0 - 1, n.1);
            if !v.contains(&new_pos) {
                v.insert(new_pos);
                q.push_back(new_pos);
            }
        }
        if n.0 < grid[0].len() as i32 - 1 && grid[n.1 as usize][(n.0 + 1) as usize] == '.' {
            let new_pos = (n.0 + 1, n.1);
            if !v.contains(&new_pos) {
                v.insert(new_pos);
                q.push_back((n.0 + 1, n.1));
            }
        }
        if n.1 > 0 && grid[(n.1 - 1) as usize][n.0 as usize] == '.' {
            let new_pos = (n.0, n.1 - 1);
            if !v.contains(&new_pos) {
                v.insert(new_pos);
                q.push_back((n.0, n.1 - 1));
            }
        }
        if n.1 < grid.len() as i32 - 1 && grid[(n.1 + 1) as usize][n.0 as usize] == '.' {
            let new_pos = (n.0, n.1 + 1);
            if !v.contains(&new_pos) {
                v.insert(new_pos);
                q.push_back((n.0, n.1 + 1));
            }
        }
    }
}

#[derive(Debug, PartialEq)]
enum Direction {
    Up,
    Down,
    Right,
    Left
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    // part_one(&lines);   // 52882: KO, 52035: OK
    part_two(&lines);
}
