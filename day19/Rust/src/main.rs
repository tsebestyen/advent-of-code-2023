use std::fs;
use std::cmp::{min, max};
use std::collections::{HashMap, VecDeque};

fn part_one(data: &str) {
    let (workflows_str, parts_str) = data.split_once("\n\n").unwrap();
    let mut parts = Vec::new();
    for line in parts_str.split("\n") {
        if line.len() == 0 {
            continue;
        }
        let new_line = &line[1..line.len() - 1];
        let tokens = new_line.split(",");
        let mut x = 0;
        let mut m = 0;
        let mut a = 0;
        let mut s = 0;
        for token in tokens {
            let (label, value_str) = token.split_once("=").unwrap();
            match label {
                "x" => x = value_str.parse().unwrap(),
                "m" => m = value_str.parse().unwrap(),
                "a" => a = value_str.parse().unwrap(),
                "s" => s = value_str.parse().unwrap(),
                _ => unreachable!("Ooops!")
            }
        }
        let part = Part { x: x, m: m, a: a, s: s };
        parts.push(part);
    }
    let mut workflows = HashMap::new();
    for line in workflows_str.split("\n") {
        let brace_idx = line.find("{").unwrap();
        let label = line[..brace_idx].to_string();
        let rules_str = &line[brace_idx + 1..line.len() - 1];
        let tokens = rules_str.split(",");
        let mut rules = Vec::new();
        for token in tokens {
            if !token.contains(":") {
                rules.push(Rule { condition: "".to_string(), destination: token.to_string() });
                continue;
            }
            let (condition, destination) = token.split_once(":").unwrap();
            rules.push(Rule { condition: condition.to_string(), destination: destination.to_string() });
        }
        workflows.insert(label, rules);
    }
    let mut accepted_parts = Vec::new();
    for part in parts {
        let mut destination = String::from("in");
        let mut final_reached = false;
        while !final_reached {
            let wf = workflows.get(&destination).unwrap();
            for rule in wf {
                let mut rule_matched = false;
                if rule.condition.is_empty() {
                    destination = rule.destination.clone();
                } else {
                    let label = rule.condition.chars().nth(0).unwrap();
                    let operator = rule.condition.chars().nth(1).unwrap();
                    let value: i32 = rule.condition[2..].parse().unwrap();
                    match (label, operator) {
                        ('x', '<') => if part.x < value { destination = rule.destination.clone(); rule_matched = true; },
                        ('x', '>') => if part.x > value { destination = rule.destination.clone(); rule_matched = true; },
                        ('m', '<') => if part.m < value { destination = rule.destination.clone(); rule_matched = true; },
                        ('m', '>') => if part.m > value { destination = rule.destination.clone(); rule_matched = true; },
                        ('a', '<') => if part.a < value { destination = rule.destination.clone(); rule_matched = true; },
                        ('a', '>') => if part.a > value { destination = rule.destination.clone(); rule_matched = true; },
                        ('s', '<') => if part.s < value { destination = rule.destination.clone(); rule_matched = true; },
                        ('s', '>') => if part.s > value { destination = rule.destination.clone(); rule_matched = true; },
                        _ => unreachable!("Oops!")
                    }
                }
                match destination.as_str() {
                    "R" => {
                        final_reached = true;
                        break;
                    }
                    "A" => {
                        final_reached = true;
                        accepted_parts.push(part);
                        break;
                    }
                    _ => ()
                }
                if rule_matched {
                    break;
                }
            }
        }
    }
    println!("{}", accepted_parts.iter().map(|p| p.x + p.m + p.a + p.s).sum::<i32>());
}

fn part_two(data: &str) {
    let (workflows_str, _) = data.split_once("\n\n").unwrap();
    let mut workflows = HashMap::new();
    for line in workflows_str.split("\n") {
        let brace_idx = line.find("{").unwrap();
        let label = line[..brace_idx].to_string();
        let rules_str = &line[brace_idx + 1..line.len() - 1];
        let tokens = rules_str.split(",");
        let mut rules = Vec::new();
        for token in tokens {
            if !token.contains(":") {
                rules.push(Rule { condition: "".to_string(), destination: token.to_string() });
                continue;
            }
            let (condition, destination) = token.split_once(":").unwrap();
            rules.push(Rule { condition: condition.to_string(), destination: destination.to_string() });
        }
        workflows.insert(label, rules);
    }

    let sr = SolutionRange {
        x_min: 1,
        x_max: 4000,
        m_min: 1,
        m_max: 4000,
        a_min: 1,
        a_max: 4000,
        s_min: 1,
        s_max: 4000,
        current_workflow: "in",
        current_rule_idx: 0,
        history: vec!["WF: in, Rule: 0".to_string()]
    };
    let mut accepted_solutions = Vec::new();
    let mut q = VecDeque::new();
    q.push_back(sr);
    while !q.is_empty() {
        let mut sr = q.pop_front().unwrap();
        let wf = workflows.get(sr.current_workflow).unwrap();
        let r = &wf[sr.current_rule_idx];

        if r.condition.is_empty() {
            if r.destination == "A" {
                //sr.history.push(format!("WF: {}, Rule: {}", sr.current_workflow, sr.current_rule_idx));
                accepted_solutions.push(sr.clone());
            } else if r.destination != "R" {
                let mut new_sr = sr.clone();
                new_sr.current_workflow = r.destination.as_str();
                new_sr.current_rule_idx = 0;
                new_sr.history.push(format!("WF: {}, Rule: {}", new_sr.current_workflow, new_sr.current_rule_idx));
                q.push_back(new_sr);
            }
        } else {
            // Apply the rule on the range and add to the queue
            let mut new_sr = sr.clone();
            let label = r.condition.chars().nth(0).unwrap();
            let operator = r.condition.chars().nth(1).unwrap();
            let value: i32 = r.condition[2..].parse().unwrap();
            match (label, operator) {
                ('x', '<') => {
                    sr.x_min = value;
                    new_sr.x_max = min(new_sr.x_max, value - 1);
                },
                ('x', '>') => {
                    sr.x_max = value;
                    new_sr.x_min = max(new_sr.x_min, value + 1);
                },
                ('m', '<') => {
                    sr.m_min = value;
                    new_sr.m_max = min(new_sr.m_max, value - 1);
                },
                ('m', '>') => {
                    sr.m_max = value;
                    new_sr.m_min = max(new_sr.m_min, value + 1);
                },
                ('a', '<') => {
                    sr.a_min = value;
                    new_sr.a_max = min(new_sr.a_max, value - 1);
                },
                ('a', '>') => {
                    sr.a_max = value;
                    new_sr.a_min = max(new_sr.a_min, value + 1);
                },
                ('s', '<') => {
                    sr.s_min = value;
                    new_sr.s_max = min(new_sr.s_max, value - 1);
                },
                ('s', '>') => {
                    sr.s_max = value;
                    new_sr.s_min = max(new_sr.s_min, value + 1);
                },
                _ => unreachable!("Oops!")
            }
            if r.destination == "A" {
                //sr.history.push(format!("WF: {}, Rule: {}", sr.current_workflow, sr.current_rule_idx));
                accepted_solutions.push(new_sr.clone());
            } else if r.destination != "R" {
                new_sr.current_workflow = r.destination.as_str();
                new_sr.current_rule_idx = 0;
                new_sr.history.push(format!("WF: {}, Rule: {}", new_sr.current_workflow, new_sr.current_rule_idx));
                q.push_back(new_sr);
            }
            
            // If rule doesn't apply -> simply go to next rule
            if sr.current_rule_idx + 1 < wf.len() {
                sr.current_rule_idx += 1;
                sr.history.push(format!("WF: {}, Rule: {}", sr.current_workflow, sr.current_rule_idx));
                q.push_back(sr);
            }
        }
    }

    // for s in &accepted_solutions {
    //     println!("x: {}\nX: {}\nm: {}\nM: {}\na: {}\nA: {}\ns: {}\nS: {}\n{:?}", 
    //         s.x_min, s.x_max, s.m_min, s.m_max, s.a_min, s.a_max, s.s_min, s.s_max, s.history);
    // }

    println!("{}", accepted_solutions.iter().map(|sr| { 
        (sr.x_max - sr.x_min + 1) as i64 *
        (sr.m_max - sr.m_min + 1) as i64 *
        (sr.a_max - sr.a_min + 1) as i64 *
        (sr.s_max - sr.s_min + 1) as i64
    }).sum::<i64>());
}

#[derive(Debug, Clone)]
struct SolutionRange<'a> {
    x_min: i32,
    x_max: i32,
    m_min: i32,
    m_max: i32,
    a_min: i32,
    a_max: i32,
    s_max: i32,
    s_min: i32,
    current_workflow: &'a str,
    current_rule_idx: usize,
    history: Vec<String>
}

#[derive(Debug, Clone)]
struct Rule {
    condition: String,
    destination: String
}

#[derive(Debug, Clone, Copy)]
struct Part {
    x: i32,
    m: i32,
    a: i32,
    s: i32
}

fn main() {
    // let data = fs::read_to_string("sample_input").expect("Unable to read input");
    let data = fs::read_to_string("input").expect("Unable to read input");
    // part_one(&data);
    part_two(&data);
}
