use std::fs;

fn part_one(lines: &Vec<&str>) {
    let mut extras = Vec::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let mut sequence: Vec<i32> = line.split_whitespace().map(|v| v.parse().unwrap()).collect();
        let mut differences = Vec::new();
        differences.push(sequence.clone());
        loop {
            sequence = sequence.iter().zip(&sequence[1..]).map(|(a, b)| b - a).collect();
            differences.push(sequence.clone());
            if differences.last().unwrap().iter().all(|&v| v == 0) {
                break;
            }
        }
        differences.reverse();
        let mut extra = 0;
        for diff in differences[1..].iter() {
            let last = diff.last().unwrap();
            extra = last + extra;
        }
        extras.push(extra);
    }
    println!("{}", extras.iter().sum::<i32>());
}

fn part_two(lines: &Vec<&str>) {
    let mut extras = Vec::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let mut sequence: Vec<i32> = line.split_whitespace().map(|v| v.parse().unwrap()).collect();
        let mut differences = Vec::new();
        differences.push(sequence.clone());
        loop {
            sequence = sequence.iter().zip(&sequence[1..]).map(|(a, b)| b - a).collect();
            differences.push(sequence.clone());
            if differences.last().unwrap().iter().all(|&v| v == 0) {
                break;
            }
        }
        differences.reverse();
        let mut extra = 0;
        for diff in differences[1..].iter() {
            let last = diff.first().unwrap();
            extra = last - extra;
        }
        extras.push(extra);
    }
    println!("{}", extras.iter().sum::<i32>());
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    
    // part_one(&lines);   // 1980437560
    part_two(&lines);      // 977
}
