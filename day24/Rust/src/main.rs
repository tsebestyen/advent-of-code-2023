use std::fs;
use itertools::Itertools;

fn part_one(lines: &Vec<&str>) {
    let mut hail_lines = Vec::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (coords_str, speeds_str) = line.trim().split_once(" @ ").unwrap();
        let coords: Vec<i128> = coords_str.splitn(3, ", ").map(|s| s.trim().parse().unwrap()).collect();
        let speeds: Vec<i128> = speeds_str.splitn(3, ", ").map(|s| s.trim().parse().unwrap()).collect();
        
        let x1 = coords[0];
        let y1 = coords[1];
        let y2 = coords[1] + speeds[1];
        let x2 = coords[0] + speeds[0];

        let a = y2 - y1;
        let b = x1 - x2;
        let c = y1 * (x2 - x1) - x1 * (y2 - y1);
        
        let hail_line = Line {
            start: (coords[0], coords[1], coords[2]),
            speeds: (speeds[0], speeds[1], speeds[2]),
            a: a,
            b: b,
            c: c
        };
        hail_lines.push(hail_line);
    }

    // let x_min = 7_f64;
    // let x_max = 27_f64;
    let x_min = 200000000000000_f64;
    let x_max = 400000000000000_f64;
    let y_min = x_min;
    let y_max = x_max;

    let mut result = 0;
    for c in hail_lines.iter().combinations(2) {
        let l1 = c[0];
        let l2 = c[1];
        let x = (l1.b * l2.c - l2.b * l1.c) as f64 / (l1.a * l2.b - l2.a * l1.b) as f64;
        let y = (l2.a * l1.c - l1.a * l2.c) as f64 / (l1.a * l2.b - l2.a * l1.b) as f64;
        if !(x_min < x && x < x_max && y_min < y && y < y_max) {
            // Outside of search area
            continue;
        }
        // Filter out past intersections for l1
        if l1.speeds.0 > 0 && x < l1.start.0 as f64
            || l1.speeds.0 < 0 && x > l1.start.0 as f64
            || l1.speeds.1 > 0 && y < l1.start.1 as f64
            || l1.speeds.1 < 0 && y > l1.start.1 as f64 {
            continue;
        }
        // Filter out past intersections for l2
        if l2.speeds.0 > 0 && x < l2.start.0 as f64
            || l2.speeds.0 < 0 && x > l2.start.0 as f64
            || l2.speeds.1 > 0 && y < l2.start.1 as f64
            || l2.speeds.1 < 0 && y > l2.start.1 as f64 {
            continue;
        }
        result += 1;
    }
    println!("{}", result);
}

#[derive(Debug)]
struct Line {
    start: (i128, i128, i128),
    speeds: (i128, i128, i128),
    a: i128,
    b: i128,
    c: i128
}

fn main() {
    // let data = fs::read_to_string("sample_input").expect("Unable to read input");
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    part_one(&lines);   // 15318
}