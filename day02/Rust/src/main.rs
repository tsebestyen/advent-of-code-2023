use std::fs;
use regex::Regex;

struct Subset {
    red: i32,
    green: i32,
    blue: i32
}

struct Game {
    id: i32,
    subsets: Vec<Subset>
}

fn part_one(games: &Vec<Game>) {
    let red = 12;
    let green = 13;
    let blue = 14;

    let result: i32 = games
        .iter()
        .filter(|g| g.subsets
                        .iter()
                        .filter(|s| s.red > red || s.green > green || s.blue > blue)
                        .count() == 0)
        .map(|g| g.id)
        .sum();

    println!("{}", result);
}

fn part_two(games: &Vec<Game>) {
    let mut result = 0;
    for game in games {
        let max_red = game.subsets.iter().map(|s| s.red).max().unwrap();
        let max_green = game.subsets.iter().map(|s| s.green).max().unwrap();
        let max_blue = game.subsets.iter().map(|s| s.blue).max().unwrap();
        let power = max_red * max_green * max_blue;
        result += power
    }
    println!("{}", result);
}

fn parse_games(lines: &Vec<&str>) -> Vec<Game> {
    let mut games = Vec::new();
    let re = Regex::new(r"Game (\d+):").unwrap();
    let re_red = Regex::new(r"(\d+) red").unwrap();
    let re_green = Regex::new(r"(\d+) green").unwrap();
    let re_blue = Regex::new(r"(\d+) blue").unwrap();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let game_capture = re.captures(line).unwrap();
        let game_id: i32 = game_capture.get(1).unwrap().as_str().parse().unwrap();
        let mut subsets = Vec::new();
        for subset in line.split(";") {
            let red_capture = re_red.captures(subset);
            let green_capture = re_green.captures(subset);
            let blue_capture = re_blue.captures(subset);
            let red: i32 = match red_capture {
                Some(c) => c.get(1).unwrap().as_str().parse().unwrap(),
                _ => 0
            };
            
            let green: i32 = match green_capture {
                Some(c) => c.get(1).unwrap().as_str().parse().unwrap(),
                _ => 0
            };
            let blue: i32 = match blue_capture {
                Some(c) => c.get(1).unwrap().as_str().parse().unwrap(),
                _ => 0
            };
            subsets.push(Subset {red, green, blue});
        }
        games.push(Game {id: game_id, subsets: subsets});
    }
    games
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    let games = parse_games(&lines);
    // part_one(&games);   // 2162
    part_two(&games);      // 72513
}
