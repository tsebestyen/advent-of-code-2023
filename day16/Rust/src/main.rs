use std::fs;
use std::collections::HashMap;

fn part_one(lines: &Vec<&str>) {
    let mut beams = vec![Beam { pos: (0, 0), direction: Direction::Right }];
    let mut visited = HashMap::<(i32, i32), bool>::new();
    visited.insert((0, 0), true);
    calculate_energy(lines, &mut beams, &mut visited);
    println!("{}", visited.iter().filter(|(_, &v)| v).count());
}

fn part_two(lines: &Vec<&str>) {
    let mut energies = Vec::new();
    // Down
    for i in 0..lines[0].len() {
        let mut beams = vec![Beam { pos: (0, i as i32), direction: Direction::Down }];
        let mut visited = HashMap::new();
        visited.insert((0, i as i32), true);
        calculate_energy(lines, &mut beams, &mut visited);
        energies.push(visited.iter().filter(|(_, &v)| v).count());
    }
    // Up
    for i in 0..lines[0].len() {
        let mut beams = vec![Beam { pos: (lines.len() as i32 - 1 , i as i32), direction: Direction::Up }];
        let mut visited = HashMap::new();
        visited.insert((lines.len() as i32 - 1, i as i32), true);
        calculate_energy(lines, &mut beams, &mut visited);
        energies.push(visited.iter().filter(|(_, &v)| v).count());
    }
    // Right
    for i in 0..lines.len() {
        let mut beams = vec![Beam { pos: (i as i32, 0), direction: Direction::Right }];
        let mut visited = HashMap::new();
        visited.insert((i as i32, 0), true);
        calculate_energy(lines, &mut beams, &mut visited);
        energies.push(visited.iter().filter(|(_, &v)| v).count());
    }
    // Left
    for i in 0..lines.len() {
        let mut beams = vec![Beam { pos: (i as i32, lines[0].len() as i32 - 1), direction: Direction::Left }];
        let mut visited = HashMap::new();
        visited.insert((i as i32, lines[0].len() as i32 - 1), true);
        calculate_energy(lines, &mut beams, &mut visited);
        energies.push(visited.iter().filter(|(_, &v)| v).count());
    }
    println!("{}", energies.iter().max().unwrap());
}

fn calculate_energy(lines: &Vec<&str>, beams: &mut Vec<Beam>, visited: &mut HashMap<(i32, i32), bool>) {
    let mut splitters = HashMap::<(i32, i32), Vec<Direction>>::new();
    while beams.len() > 0 {
        //println!("{}", beams.len());
        let mut beam = beams.pop().unwrap();
        let move_vec = match beam.direction {
            Direction::Right => (0, 1),
            Direction::Left => (0, -1),
            Direction::Up => (-1, 0),
            Direction::Down => (1, 0)
        };
        let new_pos = (beam.pos.0 + move_vec.0, beam.pos.1 + move_vec.1);
        
        if new_pos.0 < 0 || new_pos.0 > lines.len() as i32 - 1 {
            continue;
        }
        if new_pos.1 < 0 || new_pos.1 > lines[0].len() as i32 - 1 {
            continue;
        }

        visited.insert(new_pos, true);
        let tile = lines[new_pos.0 as usize].chars().nth(new_pos.1 as usize).unwrap();
        let mut readd = true;
        match (tile, &beam.direction) {
            ('.', _) => (),
            ('\\', Direction::Right) => {
                beam.direction = Direction::Down;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Down) {
                        new_prev.push(Direction::Down);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Down]);
                }
            },
            ('\\', Direction::Left) => {
                beam.direction = Direction::Up;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Up) {
                        new_prev.push(Direction::Up);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Up]);
                }
            },
            ('\\', Direction::Up) => {
                beam.direction = Direction::Left;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Left) {
                        new_prev.push(Direction::Left);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Left]);
                }
            },
            ('\\', Direction::Down) => {
                beam.direction = Direction::Right;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Right) {
                        new_prev.push(Direction::Right);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Right]);
                }
            },
            ('/', Direction::Right) => {
                beam.direction = Direction::Up;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Up) {
                        new_prev.push(Direction::Up);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Up]);
                }
            },
            ('/', Direction::Left) => {
                beam.direction = Direction::Down;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Down) {
                        new_prev.push(Direction::Down);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Down]);
                }
            },
            ('/', Direction::Up) => {
                beam.direction = Direction::Right;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Right) {
                        new_prev.push(Direction::Right);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Right]);
                }
            },
            ('/', Direction::Down) => {
                beam.direction = Direction::Left;
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Left) {
                        new_prev.push(Direction::Left);
                        splitters.insert(new_pos, new_prev);
                    } else {
                        readd = false;
                    }
                } else {
                    splitters.insert(new_pos, vec![Direction::Left]);
                }
            },
            ('-', Direction::Right | Direction::Left) => (),
            ('-', Direction::Up | Direction::Down) => {
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Left) {
                        beams.push(Beam { pos: new_pos, direction: Direction::Left });
                        new_prev.push(Direction::Left);
                        splitters.insert(new_pos, new_prev);
                    }
                } else {
                    beams.push(Beam { pos: new_pos, direction: Direction::Left });
                    splitters.insert(new_pos, vec![Direction::Left]);
                }
                beam.direction = Direction::Right;
            },
            ('|', Direction::Up | Direction::Down) => (),
            ('|', Direction::Right | Direction::Left) => {
                if splitters.contains_key(&new_pos) {
                    let prev: &Vec<Direction> = splitters.get(&new_pos).unwrap();
                    let mut new_prev = prev.clone();
                    if !prev.contains(&Direction::Down) {
                        beams.push(Beam { pos: new_pos, direction: Direction::Down });
                        new_prev.push(Direction::Down);
                        splitters.insert(new_pos, new_prev);
                    }
                } else {
                    beams.push(Beam { pos: new_pos, direction: Direction::Down });
                    splitters.insert(new_pos, vec![Direction::Down]);
                }
                beam.direction = Direction::Up;
            },
            _ => unreachable!("Ooops!")
        }
        beam.pos = new_pos;
        if readd {
            beams.push(beam);
        }
    }
}

struct Beam {
    pos: (i32, i32),
    direction: Direction
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Direction {
    Right,
    Left,
    Up,
    Down
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.trim().split("\n").collect();
    //part_one(&lines);     // 7927
    part_two(&lines);       // 8246
}
