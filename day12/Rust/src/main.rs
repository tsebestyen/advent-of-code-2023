use std::fs;
use std::cmp::min;

fn part_one(lines: &Vec<&str>) {
    let mut result: i32 = 0;
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let (record, solution) = line.split_once(" ").unwrap();
        let group_sizes: Vec<usize> = solution.trim().split(',').map(|v| v.parse().unwrap()).collect();
        let mut l_result = 0;
        solve_record(&record, &group_sizes, &mut l_result);
        //println!("{}", l_result);
        result += l_result;
    }
    println!("{}", result);
}

fn solve_record(current_pattern: &str, solutions: &Vec<usize>, result: &mut i32) {
    let qm_idx_opt = current_pattern.find('?');
    if qm_idx_opt.is_some() {
        let qm_idx = qm_idx_opt.unwrap();
        for c in vec!['#', '.'] {
            let mut new_pattern = current_pattern.to_string();
            new_pattern.replace_range(qm_idx..qm_idx + 1, &c.to_string());
            let dot_idx = new_pattern[qm_idx + 1..].find('.').unwrap_or(new_pattern.len());
            let new_qm_idx = new_pattern.find('?').unwrap_or(new_pattern.len());
            let cut_idx = min(new_qm_idx, dot_idx);
            let groups: Vec<usize> = new_pattern[..cut_idx].split('.').filter(|t| !t.is_empty()).map(|v| v.len()).collect();
            // println!("{}", new_pattern);
            // println!("{:?}", groups);
            if !groups.iter().zip(solutions).all(|(a, b)| a == b) {
                if groups.len() <= solutions.len() && solutions[groups.len() - 1] > groups[groups.len() - 1] {
                    solve_record(&new_pattern, &solutions, result);
                }
                continue;
            }
            solve_record(&new_pattern, &solutions, result);
        }
    } else {
        let groups: Vec<usize> = current_pattern.split('.').filter(|t| !t.is_empty()).map(|v| v.len()).collect();
        if &groups != solutions {
            return;
        } else {
            //println!("{}", current_pattern);
            *result += 1;
        }

    }
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    part_one(&lines);
}
