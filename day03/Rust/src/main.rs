use std::fs;
use regex::Regex;

fn part_one(lines: &Vec<&str>) {
    let mut result = 0;
    let re = Regex::new(r"(\d+)").unwrap();
    let re_symbol = Regex::new(r"[^0-9\.]").unwrap();
    for (row_idx, row) in lines.iter().enumerate() {
        if row.len() == 0 {
            continue;
        }
        let num_capture = re.captures_iter(row);
        for cap in num_capture {
            let m = cap.get(1).unwrap();
            let number = m.as_str().parse::<i32>().unwrap();
            let start = if m.start() > 0 { m.start() - 1 } else { m.start() };
            let end = if m.end() < row.len() - 1 { m.end() + 1 } else { m.end() };

            // Previous row
            if row_idx > 0 {
                let s = &lines[row_idx - 1][start..end];
                if re_symbol.is_match(s) {
                    result += number;
                }
            }
            // Current row
            let start_char = row.chars().nth(start).unwrap();
            let end_char = row.chars().nth(end - 1).unwrap();
            if !start_char.is_digit(10) && start_char != '.' {
                result += number;
            }
            if !end_char.is_digit(10) && end_char != '.' {
                result += number;
            }
            // Next row
            if row_idx < lines.len() - 1 {
                if lines[row_idx + 1].len() > 0 {
                    let s = &lines[row_idx + 1][start..end];
                    if re_symbol.is_match(s) {
                        result += number;
                    }
                }
            }
        }
    }

    println!("{}", result);
}

fn part_two(lines: &Vec<&str>) {
    let mut result = 0;
    let re = Regex::new(r"(\d+)").unwrap();
    let re_symbol = Regex::new(r"([\*])").unwrap();
    for (row_idx, row) in lines.iter().enumerate() {
        if row.len() == 0 {
            continue;
        }
        let gear_capture = re_symbol.captures_iter(row);
        for cap in gear_capture {
            let m = cap.get(1).unwrap();
            let start = m.start();

            let mut numbers = Vec::new();

            // Previous row
            if row_idx > 0 {
                let num_capture = re.captures_iter(lines[row_idx - 1]);
                for num in num_capture {
                    let m = num.get(1).unwrap();
                    let num_start = m.start();
                    let num_end = m.end() - 1;
                    let value: usize = m.as_str().parse().unwrap();
                    if num_end == start - 1
                        || (num_start <= start && num_end >= start)
                        || num_start == start + 1 {
                            numbers.push(value);
                        }
                }
            }
            // Current row
            let num_capture = re.captures_iter(row);
            for num in num_capture {
                let m = num.get(1).unwrap();
                let num_start = m.start();
                let num_end = m.end() - 1;
                let value: usize = m.as_str().parse().unwrap();
                if num_end == start - 1
                    || num_start == start + 1 {
                        numbers.push(value);
                    }
            }
            // Next row
            if row_idx < lines.len() - 1 {
                let num_capture = re.captures_iter(lines[row_idx + 1]);
                for num in num_capture {
                    let m = num.get(1).unwrap();
                    let num_start = m.start();
                    let num_end = m.end() - 1;
                    let value: usize = m.as_str().parse().unwrap();
                    if num_end == start - 1
                        || (num_start <= start && num_end >= start)
                        || num_start == start + 1 {
                            numbers.push(value);
                        }
                }
            }

            if numbers.len() == 2 {
                //println!("{}, {}", numbers[0], numbers[1]);
                result += numbers[0] * numbers[1];
            }
        }
    }
    println!("{}", result);
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    // part_one(&lines);       // 539637
    part_two(&lines);   // 82114114: KO, 82950621: KO, 68676184: KO, 82818007: OK
}
