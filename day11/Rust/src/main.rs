use std::fs;
use itertools::Itertools;

fn part_one(lines: &Vec<&str>) {
    let mut row_expanded = Vec::new();
    let mut idx = 0;
    // Expand rows
    while idx < lines.len() {
        let row = &lines[idx].trim();
        if row.len() == 0 {
            break;
        }
        if row.chars().all(|c| c == '.') {
            row_expanded.push(row.to_string());
        }
        row_expanded.push(row.to_string());
        idx += 1;
    }
    // Expand cols
    idx = 0;
    let mut col_expanded = Vec::new();
    let num_cols = row_expanded[0].trim().len();
    while idx < num_cols {
        let col: String = row_expanded.iter().map(|l| l.chars().nth(idx).unwrap()).collect();
        if col.chars().all(|c| c == '.') {
            col_expanded.push(col.clone());
        }
        col_expanded.push(col);
        idx += 1;
    }

    // Get back original orientation
    let mut expanded = Vec::new();
    let num_cols = col_expanded[0].trim().len();
    idx = 0;
    while idx < num_cols {
        let row: String = col_expanded.iter().map(|c| c.chars().nth(idx).unwrap()).collect();
        expanded.push(row);
        idx += 1;
    }

    // for row in &expanded {
    //     println!("{:?}", row);
    // }

    let mut galaxy_coords = Vec::new();
    for (row_idx, row) in expanded.iter().enumerate() {
        for (col_idx, value) in row.chars().enumerate() {
            if value == '#' {
                galaxy_coords.push((row_idx as i128, col_idx as i128));
            }
        }
    }
    let mut result = 0;
    let galaxy_pairs = galaxy_coords.iter().combinations(2);
    for pair in galaxy_pairs {
        let galaxy_1_coords = pair[0];
        let galaxy_2_coords = pair[1];
        let distance = get_manhattan_distance(galaxy_1_coords, galaxy_2_coords);
        result += distance;
    }
    println!("{}", result);
}

fn part_two(lines: &Vec<&str>) {
    let mut row_expand_indexes = Vec::new();
    let mut col_expand_indexes = Vec::new();
    let mut idx = 0;
    while idx < lines.len() {
        let row = &lines[idx].trim();
        if row.len() == 0 {
            break;
        }
        if row.chars().all(|c| c == '.') {
            row_expand_indexes.push(idx);
        }
        idx += 1;
    }
    idx = 0;
    let num_cols = lines[0].trim().len();
    while idx < num_cols {
        let col: String = lines.iter().filter(|l| !l.is_empty()).map(|l| l.chars().nth(idx).unwrap()).collect();
        if col.chars().all(|c| c == '.') {
            col_expand_indexes.push(idx);
        }
        idx += 1;
    }
    let mut galaxy_coords: Vec<(i128, i128)> = Vec::new();
    for (row_idx, row) in lines.iter().enumerate() {
        for (col_idx, value) in row.chars().enumerate() {
            if value == '#' {
                let x_expanson = col_expand_indexes.iter().filter(|&idx| *idx < col_idx).count();
                let y_expanson = row_expand_indexes.iter().filter(|&idx| *idx < row_idx).count();
                let coord = (y_expanson as i128 * 1_000_000 + row_idx as i128 - y_expanson as i128, x_expanson as i128 * 1_000_000 + col_idx as i128 - x_expanson as i128);
                galaxy_coords.push(coord);
            }
        }
    }
    let mut result = 0;
    let galaxy_pairs = galaxy_coords.iter().combinations(2);
    for pair in galaxy_pairs {
        let galaxy_1_coords = pair[0];
        let galaxy_2_coords = pair[1];
        let distance = get_manhattan_distance(galaxy_1_coords, galaxy_2_coords);
        result += distance;
    }
    println!("{}", result);

}

fn get_manhattan_distance(p1: &(i128, i128), p2: &(i128, i128)) -> i128 {
    (p1.0 - p2.0).abs() + (p1.1 - p2.1).abs()
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    
    // part_one(&lines);   // 10422930
    part_two(&lines);      // 699901801146: ko, 699909023130
}
