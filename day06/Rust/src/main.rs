use std::fs;

fn part_one(races: &Vec<Race>) {
    let mut wins = Vec::new();
    for race in races {
        let mut win = 0;
        for b in 0..race.time {
            let distance = (race.time - b) * b;
            if distance > race.distance {
                win += 1;
            }
        }
        if win > 0 {
            wins.push(win);
        }
    }
    println!("{}", wins.iter().product::<i32>());
}

fn part_two(races: &Vec<Race>) {
    let mut wins = 0;
    let time: i128 = races.iter().map(|r| r.time.to_string()).collect::<Vec<String>>().join("").as_str().parse().unwrap();
    let record_distance: i128 = races.iter().map(|r| r.distance.to_string()).collect::<Vec<String>>().join("").as_str().parse().unwrap();
    for b in 0..time {
        let distance = (time - b) * b;
        if distance > record_distance {
            wins += 1;
        }
    }
    println!("{}", wins);
}

struct Race {
    time: i32,
    distance: i32
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    let times: Vec<i32> = lines[0][9..].split_whitespace().map(|v| v.parse().unwrap()).collect();
    let distances: Vec<i32> = lines[1][9..].split_whitespace().map(|v| v.parse().unwrap()).collect();
    let races = times.iter().zip(&distances).map(|z| Race {time: *z.0, distance: *z.1}).collect();
    // part_one(&races);   // 4403592
    part_two(&races);      // 38017587
}
