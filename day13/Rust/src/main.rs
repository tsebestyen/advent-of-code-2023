use std::fs;

fn part_one(patterns: &Vec<&str>) {
    let mut mirror_rows = Vec::new();
    let mut mirror_cols = Vec::new();
    for pattern in patterns {
        if pattern.len() == 0 {
            continue;
        }
        let rows: Vec<_> = pattern.split('\n').filter(|r| !r.is_empty()).collect();
        // Horizontal
        for i in 1..rows.len() {
            if rows[i..rows.len()].iter().zip(rows[0..i].iter().rev()).all(|(a, b)| a == b) {
                mirror_rows.push(i);
                break;
            }
        }
        // Vertical
        let cols: Vec<String> = (0..rows[0].len()).map(|i| rows.iter().map(|r| r.chars().nth(i).unwrap()).collect()).collect();
        for i in 1..cols.len() {
            if cols[i..cols.len()].iter().zip(cols[0..i].iter().rev()).all(|(a, b)| a == b) {
                mirror_cols.push(i);
                break;
            }
        }
    }
    let result: usize = mirror_rows.iter().sum::<usize>() * 100 + mirror_cols.iter().sum::<usize>();
    println!("{}", result);
}

fn part_two(patterns: &Vec<&str>) {
    let mut mirror_rows = Vec::new();
    let mut mirror_cols = Vec::new();
    for pattern in patterns {
        if pattern.len() == 0 {
            continue;
        }

        let mut original_row = 0;
        let mut original_col = 0;

        // Find original reflection
        let rows: Vec<_> = pattern.split('\n').filter(|r| !r.is_empty()).collect();
        // Horizontal
        for i in 1..rows.len() {
            if rows[i..rows.len()].iter().zip(rows[0..i].iter().rev()).all(|(a, b)| a == b) {
                original_row = i;
                break;
            }
        }
        // Vertical
        let cols: Vec<String> = (0..rows[0].len()).map(|i| rows.iter().map(|r| r.chars().nth(i).unwrap()).collect()).collect();
        for i in 1..cols.len() {
            if cols[i..cols.len()].iter().zip(cols[0..i].iter().rev()).all(|(a, b)| a == b) {
                original_col = i;
                break;
            }
        }

        let mut idx = 0;
        let mut found = false;
        while idx < pattern.len() {
            if found {
                break;
            }
            let mut new_pattern = pattern.to_string();
            let current_char = new_pattern.chars().nth(idx).unwrap();
            let mut new_char = current_char;
            if current_char == '#' {
                new_char = '.';
            } else if current_char == '.' {
                new_char = '#';
            }

            new_pattern.replace_range(idx..idx + 1, &new_char.to_string());
            
            let rows: Vec<_> = new_pattern.split('\n').filter(|r| !r.is_empty()).collect();
            // Horizontal
            for i in 1..rows.len() {
                if rows[i..rows.len()].iter().zip(rows[0..i].iter().rev()).all(|(a, b)| a == b) {
                    if i != original_row {
                        mirror_rows.push(i);
                        found = true;
                        break;
                    }
                }
            }
            if found {
                break;
            }
            // Vertical
            let cols: Vec<String> = (0..rows[0].len()).map(|i| rows.iter().map(|r| r.chars().nth(i).unwrap()).collect()).collect();
            for i in 1..cols.len() {
                if cols[i..cols.len()].iter().zip(cols[0..i].iter().rev()).all(|(a, b)| a == b) {
                    if i != original_col {
                        mirror_cols.push(i);
                        found = true;
                        break;
                    }
                }
            }

            idx += 1;
        }
    }
    let result: usize = mirror_rows.iter().sum::<usize>() * 100 + mirror_cols.iter().sum::<usize>();
    println!("{}", result);
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let patterns: Vec<&str> = data.split("\n\n").collect();
    // part_one(&patterns);    // 37975
    part_two(&patterns);       // 32497
}
