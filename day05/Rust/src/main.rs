use std::fs;
use std::collections::HashMap;
use std::ops::Range;

fn part_one(lines: &Vec<&str>) {
    let (seeds, maps) = parse(lines);

    let mut result = i128::MAX;

    for seed in seeds {
        let mut value = seed;
        for map in maps.iter() {
            value = map_source(value, &map);
        }
        if (value as i128) < result {
            result = value as i128;
        }
    }

    println!("{}", result);
}

fn part_two(lines: &Vec<&str>) {
    let (seeds, maps) = parse(lines);

    let mut result = i128::MAX;
    let seed_ranges: Vec<_> = seeds.chunks(2).map(|s| s[0]..s[0] + s[1]).collect();
    for (seed_range_id, seed_range) in seed_ranges.into_iter().enumerate() {
        println!("Processing seed range: {}", seed_range_id);
        for seed in seed_range {
            let mut value = seed;
            for map in maps.iter() {
                value = map_source(value, &map);
            }
            if (value as i128) < result {
                result = value as i128;
            }
        }
        println!("Min: {}", result);
    }
    println!("Min: {}", result);
}

fn map_source(source: usize, map: &HashMap<Range<usize>, Range<usize>>) -> usize {
    for r in map.iter() {
        if r.0.contains(&source) {
            let destination = r.1.start + source - r.0.start;
            return  destination;
        }
    }
    source
}

fn parse(lines: &Vec<&str>) -> (Vec<usize>, Vec<HashMap<Range<usize>, Range<usize>>>) {
    let mut seeds: Vec<usize> = Vec::new();
    let mut seed_to_soil_map = HashMap::new();
    let mut soil_to_fertilizer_map = HashMap::new();
    let mut fertilizer_to_water_map = HashMap::new();
    let mut water_to_light_map = HashMap::new();
    let mut light_to_temp_map = HashMap::new();
    let mut temp_to_humidity_map = HashMap::new();
    let mut humidity_to_location_map = HashMap::new();
    let mut seed_to_soil = false;
    let mut soil_to_fertilizer = false;
    let mut fertilizer_to_water = false;
    let mut water_to_light = false;
    let mut light_to_temp = false;
    let mut temp_to_humidity = false;
    let mut humidity_to_location = false;
    
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        if line.starts_with("seeds: ") {
            seeds = line[7..].trim().split(" ").map(|v| v.parse::<usize>().unwrap()).collect();
        }
        if line.starts_with("seed-to-soil") {
            seed_to_soil = true;
        }
        if line.starts_with("soil-to-fertilizer") {
            seed_to_soil = false;
            soil_to_fertilizer = true;
        }
        if line.starts_with("fertilizer-to-water") {
            soil_to_fertilizer = false;
            fertilizer_to_water = true;
        }
        if line.starts_with("water-to-light") {
            fertilizer_to_water = false;
            water_to_light = true;
        }
        if line.starts_with("light-to-temperature") {
            water_to_light = false;
            light_to_temp = true;
        }
        if line.starts_with("temperature-to-humidity") {
            light_to_temp = false;
            temp_to_humidity = true;
        }
        if line.starts_with("humidity-to-location map") {
            temp_to_humidity = false;
            humidity_to_location = true;
        }
        if line.chars().nth(0).unwrap().is_digit(10) {
            let numbers: Vec<usize> = line.trim().split(" ").map(|v| v.parse().unwrap()).collect();
            let (d, s, r) = (numbers[0], numbers[1], numbers[2]);
            if seed_to_soil {
                seed_to_soil_map.insert(s..s + r, d..d + r);
            }
            if soil_to_fertilizer {
                soil_to_fertilizer_map.insert(s..s + r, d..d + r);
            }
            if fertilizer_to_water {
                fertilizer_to_water_map.insert(s..s + r, d..d + r);
            }
            if water_to_light {
                water_to_light_map.insert(s..s + r, d..d + r);
            }
            if light_to_temp {
                light_to_temp_map.insert(s..s + r, d..d + r);
            }
            if temp_to_humidity {
                temp_to_humidity_map.insert(s..s + r, d..d + r);
            }
            if humidity_to_location {
                humidity_to_location_map.insert(s..s + r, d..d + r);
            }
        }
    }

    let maps = vec![seed_to_soil_map,
                    soil_to_fertilizer_map,
                    fertilizer_to_water_map,
                    water_to_light_map,
                    light_to_temp_map,
                    temp_to_humidity_map,
                    humidity_to_location_map];

    (seeds, maps)
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    // part_one(&lines);   // 174137457
    part_two(&lines);   // 58880743: KO, 1493866: OK
}
