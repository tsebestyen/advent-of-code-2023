use std::fs;

fn part_one(lines: &Vec<&str>) {
    let mut area = Vec::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let mut row = Vec::new();
        for c in line.trim().chars() {
            let spot = match c {
                'O' => Spot::RoundRock,
                '#' => Spot::CubeRock,
                _ => Spot::Empty
            };
            row.push(spot);
        }
        area.push(row);
    }

    // Roll the rocks
    for i in 1..area.len() {
        for spot_idx in 0..area[0].len() {
            for n in 0..i {
                match (&area[i - n][spot_idx], &area[i - n - 1][spot_idx]) {
                    (Spot::RoundRock, Spot::Empty) => {
                        area[i - n][spot_idx] = Spot::Empty;
                        area[i - n - 1][spot_idx] = Spot::RoundRock; 
                    },
                    _ => ()
                }
            }
        }
    }

    let mut result = 0;
    // Count
    for i in 0..area.len() {
        for s in 0..area[0].len() {
            match area[i][s] {
                Spot::RoundRock => result += area.len() - i,
                _ => ()
            }
        }
    }
    println!("{}", result);
}

fn part_two(lines: &Vec<&str>) {
    let mut area = Vec::new();
    for line in lines {
        if line.len() == 0 {
            continue;
        }
        let mut row = Vec::new();
        for c in line.trim().chars() {
            let spot = match c {
                'O' => Spot::RoundRock,
                '#' => Spot::CubeRock,
                _ => Spot::Empty
            };
            row.push(spot);
        }
        area.push(row);
    }

    let mut history: Vec<Vec<Vec<Spot>>> = Vec::new();

    let mut idx = 1;
    let mut loop_start = 0;
    let mut loop_length = 0;
    let mut loop_used = false;
    let mut num_cycles = 1_000_000_000;
    while idx < num_cycles + 1 {
        // North
        for i in 1..area.len() {
            for spot_idx in 0..area[0].len() {
                for n in 0..i {
                    match (&area[i - n][spot_idx], &area[i - n - 1][spot_idx]) {
                        (Spot::RoundRock, Spot::Empty) => {
                            area[i - n][spot_idx] = Spot::Empty;
                            area[i - n - 1][spot_idx] = Spot::RoundRock; 
                        },
                        _ => ()
                    }
                }
            }
        }

        // West
        for i in 0..area.len() {
            for spot_idx in 1..area[0].len() {
                for n in 0..spot_idx {
                    match (&area[i][spot_idx - n], &area[i][spot_idx - n - 1]) {
                        (Spot::RoundRock, Spot::Empty) => {
                            area[i][spot_idx - n] = Spot::Empty;
                            area[i][spot_idx - n - 1] = Spot::RoundRock; 
                        },
                        _ => ()
                    }
                }
            }
        }

        // South
        for i in 0..area.len() - 1 {
            for spot_idx in 0..area[0].len() {
                let r = area.len() - 1 - i;
                for n in 0..area.len() - r {
                    match (&area[r + n][spot_idx], &area[r + n - 1][spot_idx]) {
                        (Spot::Empty, Spot::RoundRock) => {
                            area[r + n][spot_idx] = Spot::RoundRock;
                            area[r + n - 1][spot_idx] = Spot::Empty; 
                        },
                        _ => ()
                    }
                }
            }
        }

        // East
        for i in 0..area.len() {
            for spot_idx in (0..area[0].len() - 1).rev() {
                for n in 0..area.len() - spot_idx - 1 {
                    match (&area[i][spot_idx + n], &area[i][spot_idx + n + 1]) {
                        (Spot::RoundRock, Spot::Empty) => {
                            area[i][spot_idx + n] = Spot::Empty;
                            area[i][spot_idx + n + 1] = Spot::RoundRock; 
                        },
                        _ => ()
                    }
                }
            }
        }

        // Find loop
        if loop_start == 0 && loop_length == 0 {
            for (history_idx, l_history) in history.iter().enumerate() {
                if *l_history == area {
                    //println!("Cycle {}, History {}", cycle, history_idx);
                    if loop_start == 0 {
                        loop_start = history_idx + 1;
                        loop_length = idx - loop_start;
                        break;
                    }
                    break;
                }
            }
            let mut area_clone = Vec::new();
            for row in &area {
                area_clone.push(row.clone())
            }
            history.push(area_clone);
        } else if !loop_used {
            num_cycles = (num_cycles - loop_start) % loop_length;
            area = history[loop_start - 1].clone();
            loop_used = true;
            idx = 1;
            continue;
        }

        idx += 1;
    }

    let mut result = 0;
    // Count
    for i in 0..area.len() {
        for s in 0..area[0].len() {
            match area[i][s] {
                Spot::RoundRock => result += area.len() - i,
                _ => ()
            }
        }
    }
    println!("{}", result);
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum Spot {
    Empty,
    CubeRock,
    RoundRock
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    // part_one(&lines);       // 108641
    part_two(&lines);       // 84328
}
