use std::fs;
use std::collections::{HashSet, VecDeque};

fn part_one(lines: &Vec<&str>) {
    let max_x = lines[0].len() as i32;
    let max_y = lines.len() as i32;
    let mut paths = Vec::new();

    // Get start position and the possible directions to start from there
    for (row_idx, line) in lines.iter().enumerate() {
        for (col_idx, pipe) in line.chars().enumerate() {
            let row_idx_i32 = row_idx as i32;
            let col_idx_i32 = col_idx as i32;
            if pipe == 'S' {
                // Calculate edges for start
                if row_idx_i32 > 0 {
                    if vec!['|', 'F', '7'].contains(&lines[row_idx - 1].chars().nth(col_idx).unwrap()) {
                        paths.push(((row_idx_i32 - 1, col_idx_i32), Direction::North));
                    }
                }
                if row_idx_i32 < max_y - 1 {
                    if vec!['|', 'L', 'J'].contains(&lines[row_idx + 1].chars().nth(col_idx).unwrap()) {
                        paths.push(((row_idx_i32 + 1, col_idx_i32), Direction::South));
                    }
                }
                if col_idx_i32 > 0 {
                    if vec!['-', 'L', 'F'].contains(&lines[row_idx].chars().nth(col_idx - 1).unwrap()) {
                        paths.push(((row_idx_i32, col_idx_i32 - 1), Direction::West));
                    }
                }
                if col_idx_i32 < max_x - 1 {
                    if vec!['-', 'J', '7'].contains(&lines[row_idx].chars().nth(col_idx + 1).unwrap()) {
                        paths.push(((row_idx_i32, col_idx_i32 + 1), Direction::East));
                    }
                }
                break;
            }
        }
    }

    // Try each possible start path and walk through them
    let mut distances = Vec::new();
    for path in paths {
        let mut pos = path.0;
        let mut dir = path.1;
        let mut distance = 1;
        loop {
            let pipe = lines[pos.0 as usize].chars().nth(pos.1 as usize).unwrap();

            match (&pipe, &dir) {
                ('|', Direction::North) => pos = (pos.0 - 1, pos.1),
                ('|', Direction::South) => pos = (pos.0 + 1, pos.1),
                ('-', Direction::East) => pos = (pos.0, pos.1 + 1),
                ('-', Direction::West) => pos = (pos.0, pos.1 - 1),
                ('L', Direction::South) => { pos = (pos.0, pos.1 + 1); dir = Direction::East },
                ('L', Direction::West) => { pos = (pos.0 - 1, pos.1); dir = Direction::North },
                ('J', Direction::South) => { pos = (pos.0, pos.1 - 1); dir = Direction::West },
                ('J', Direction::East) => { pos = (pos.0 - 1, pos.1); dir = Direction::North },
                ('F', Direction::North) => { pos = (pos.0, pos.1 + 1); dir = Direction::East },
                ('F', Direction::West) => { pos = (pos.0 + 1, pos.1); dir = Direction::South },
                ('7', Direction::North) => { pos = (pos.0, pos.1 - 1); dir = Direction::West },
                ('7', Direction::East) => { pos = (pos.0 + 1, pos.1); dir = Direction::South },
                ('S', _) => break,
                ('.', _) => break,
                _ => unreachable!("Wrong pipe/direction pair!")
            };

            distance += 1;
        }
        distances.push(distance);
    }

    println!("{:?}", distances.iter().max().unwrap() / 2);
}

fn part_two(lines: &Vec<&str>) {
    let max_x = lines[0].len() as i32;
    let max_y = lines.iter().filter(|l| !l.is_empty()).count() as i32;
    let mut paths = Vec::new();

    // Get start position and the possible directions to start from there
    for (row_idx, line) in lines.iter().enumerate() {
        for (col_idx, pipe) in line.chars().enumerate() {
            let row_idx_i32 = row_idx as i32;
            let col_idx_i32 = col_idx as i32;
            if pipe == 'S' {
                // Calculate edges for start
                if row_idx_i32 > 0 {
                    if vec!['|', 'F', '7'].contains(&lines[row_idx - 1].chars().nth(col_idx).unwrap()) {
                        paths.push(((row_idx_i32 - 1, col_idx_i32), Direction::North));
                    }
                }
                if row_idx_i32 < max_y - 1 {
                    if vec!['|', 'L', 'J'].contains(&lines[row_idx + 1].chars().nth(col_idx).unwrap()) {
                        paths.push(((row_idx_i32 + 1, col_idx_i32), Direction::South));
                    }
                }
                if col_idx_i32 > 0 {
                    if vec!['-', 'L', 'F'].contains(&lines[row_idx].chars().nth(col_idx - 1).unwrap()) {
                        paths.push(((row_idx_i32, col_idx_i32 - 1), Direction::West));
                    }
                }
                if col_idx_i32 < max_x - 1 {
                    if vec!['-', 'J', '7'].contains(&lines[row_idx].chars().nth(col_idx + 1).unwrap()) {
                        paths.push(((row_idx_i32, col_idx_i32 + 1), Direction::East));
                    }
                }
                break;
            }
        }
    }

    let mut main_loop_coords = Vec::new();
    let path = paths.pop().unwrap();
    let mut pos = path.0;
    let mut dir = path.1;
    let mut enclosed_grounds_left = HashSet::new();
    let mut enclosed_grounds_right = HashSet::new();
    loop {
        main_loop_coords.push(pos);
        let pipe = lines[pos.0 as usize].chars().nth(pos.1 as usize).unwrap();

        match (&pipe, &dir) {
            ('|', Direction::North) => {
                enclosed_grounds_left.insert((pos.0, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0, pos.1 + 1));
                pos = (pos.0 - 1, pos.1);
            },
            ('|', Direction::South) => {
                enclosed_grounds_left.insert((pos.0, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0, pos.1 - 1));
                pos = (pos.0 + 1, pos.1);
            },
            ('-', Direction::East) => {
                enclosed_grounds_left.insert((pos.0 - 1, pos.1));
                enclosed_grounds_right.insert((pos.0 + 1, pos.1));
                pos = (pos.0, pos.1 + 1);
            },
            ('-', Direction::West) => {
                enclosed_grounds_left.insert((pos.0 + 1, pos.1));
                enclosed_grounds_right.insert((pos.0 - 1, pos.1));
                pos = (pos.0, pos.1 - 1);
            },
            ('L', Direction::South) => {
                enclosed_grounds_left.insert((pos.0 - 1, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0 + 1, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0 + 1, pos.1));
                pos = (pos.0, pos.1 + 1);
                dir = Direction::East;
            },
            ('L', Direction::West) => {
                enclosed_grounds_right.insert((pos.0 - 1, pos.1 + 1));
                enclosed_grounds_left.insert((pos.0 + 1, pos.1));
                enclosed_grounds_left.insert((pos.0 + 1, pos.1 - 1));
                enclosed_grounds_left.insert((pos.0 - 1, pos.1 - 1));
                pos = (pos.0 - 1, pos.1);
                dir = Direction::North;
            },
            ('J', Direction::South) => {
                enclosed_grounds_right.insert((pos.0 - 1, pos.1 - 1));
                enclosed_grounds_left.insert((pos.0, pos.1 + 1));
                enclosed_grounds_left.insert((pos.0 + 1, pos.1 + 1));
                enclosed_grounds_left.insert((pos.0 + 1, pos.1));
                pos = (pos.0, pos.1 - 1);
                dir = Direction::West;
            },
            ('J', Direction::East) => {
                enclosed_grounds_left.insert((pos.0 - 1, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0 + 1, pos.1));
                enclosed_grounds_right.insert((pos.0 + 1, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0, pos.1 + 1));
                pos = (pos.0 - 1, pos.1);
                dir = Direction::North;
            },
            ('F', Direction::North) => {
                enclosed_grounds_right.insert((pos.0 + 1, pos.1 + 1));
                enclosed_grounds_left.insert((pos.0, pos.1 - 1));
                enclosed_grounds_left.insert((pos.0 - 1, pos.1 - 1));
                enclosed_grounds_left.insert((pos.0 - 1, pos.1));
                pos = (pos.0, pos.1 + 1);
                dir = Direction::East;
            },
            ('F', Direction::West) => {
                enclosed_grounds_left.insert((pos.0 + 1, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0 - 1, pos.1));
                enclosed_grounds_right.insert((pos.0 - 1, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0, pos.1 - 1));
                pos = (pos.0 + 1, pos.1);
                dir = Direction::South;
            },
            ('7', Direction::North) => {
                enclosed_grounds_left.insert((pos.0 + 1, pos.1 - 1));
                enclosed_grounds_right.insert((pos.0, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0 - 1, pos.1 + 1));
                enclosed_grounds_right.insert((pos.0 - 1, pos.1));
                pos = (pos.0, pos.1 - 1);
                dir = Direction::West;
            },
            ('7', Direction::East) => {
                enclosed_grounds_right.insert((pos.0 + 1, pos.1 - 1));
                enclosed_grounds_left.insert((pos.0 - 1, pos.1));
                enclosed_grounds_left.insert((pos.0 - 1, pos.1 + 1));
                enclosed_grounds_left.insert((pos.0, pos.1 + 1));
                pos = (pos.0 + 1, pos.1); 
                dir = Direction::South;
            },
            ('S', _) => break,
            ('.', _) => break,
            _ => unreachable!("Wrong pipe/direction pair!")
        }
    }

    // Prepare a map where all coords outside main loop are ground
    let mut main_loop_map = Vec::new();
    for (row_idx, line) in lines.iter().enumerate() {
        if line.len() == 0 {
            continue;
        }
        let mut row = String::with_capacity(max_x as usize);
        (0..max_x).for_each(|_| row.push('.'));
        for (col_idx, _) in line.chars().enumerate() {
            let row_idx_i32 = row_idx as i32;
            let col_idx_i32 = col_idx as i32;
            if main_loop_coords.contains(&(row_idx_i32, col_idx_i32)) {
                let pipe = lines[row_idx].chars().nth(col_idx).unwrap();
                row.replace_range(col_idx..col_idx + 1, &pipe.to_string());
            }
        }
        main_loop_map.push(row);
    }

    let mut min_y = 1000;
    // Get a top-most loop item
    for coord in main_loop_coords {
        if coord.0 < min_y {
            min_y = coord.0;
        }
    }
    let mut right_is_outside = true;
    if enclosed_grounds_left.iter().any(|c| c.0 == min_y - 1) {
        right_is_outside = false;
    }

    let enclosed_grounds = if right_is_outside { enclosed_grounds_left } else { enclosed_grounds_right };

    let mut result = 0;
    let mut v = HashSet::new();
    for g in enclosed_grounds {
        if g.0 < 0 || g.1 < 0 {
            continue;
        }
        if g.0 > main_loop_map.len() as i32 - 1 || g.1 > main_loop_map[0].len() as i32 - 1 {
            continue;
        }
        // if main_loop_map[g.0 as usize].chars().nth(g.1 as usize).unwrap() == '.' {
        //     main_loop_map[g.0 as usize].replace_range(g.1 as usize..g.1 as usize + 1, "I");
        // }
        if main_loop_map[g.0 as usize].chars().nth(g.1 as usize).unwrap() == '.' {
            let area = flood_fill(&main_loop_map, g, &mut v);
            result += area;
        }
    }

    println!("{}", result);
}

fn flood_fill(grid: &Vec<String>, pos: (i32, i32), v: &mut HashSet<(i32, i32)>) -> i32 {
    let mut q = VecDeque::new();
    if v.contains(&pos) {
        return 0;
    }
    q.push_back(pos);
    v.insert(pos);
    let mut result = 0;
    while !q.is_empty() {
        let n = q.pop_front().unwrap();
        if grid[n.0 as usize].chars().nth(n.1 as usize).unwrap() == '.' {
            result += 1;
        }
        if n.1 > 1 && grid[n.0 as usize].chars().nth((n.1 - 1) as usize).unwrap() == '.' {
            let new_coord = (n.0, n.1 - 1);
            if !v.contains(&new_coord) {
                q.push_back(new_coord);
                v.insert(new_coord);
            }
        }
        if n.1 < grid[0].len() as i32 - 1 && grid[n.0 as usize].chars().nth(n.1 as usize + 1).unwrap() == '.' {
            let new_coord = (n.0, n.1 + 1);
            if !v.contains(&new_coord) {
                q.push_back(new_coord);
                v.insert(new_coord);
            }
        }
        if n.0 > 1 && grid[(n.0 - 1) as usize].chars().nth(n.1 as usize).unwrap() == '.' {
            let new_coord = (n.0 -1, n.1);
            if !v.contains(&new_coord) {
                q.push_back(new_coord);
                v.insert(new_coord);
            }
        }
        if n.0 < grid.len() as i32 - 1 && grid[n.0 as usize + 1].chars().nth(n.1 as usize).unwrap() == '.' {
            let new_coord = (n.0 + 1, n.1);
            if !v.contains(&new_coord) {
                q.push_back(new_coord);
                v.insert(new_coord);
            }
        }
    }
    result
}

enum Direction {
    North,
    South,
    East,
    West
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    let lines: Vec<&str> = data.split("\n").collect();
    
    // part_one(&lines);   // 6714
    part_two(&lines);      // 615: KO, 271: ko, 384: Ko, 322: Ko
}
